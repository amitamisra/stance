#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_db
----------------------------------

Tests for `stance.db` module.
Separate from normal stance module tests because not all users may have a database set up.
"""

import unittest

from stance.db import *


class TestDb(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_db_connection(self):
        sql_engine.execute("select * from posts limit 1")

    correct_string = "home schooling is one of the worst things that a parent can do to a child. " \
                     "one of the purposes of going to school is learning how to cope with other kids " \
                     "your age. if a kid is cooped up inside his house all the time with a parent " \
                     "teaching him (who most likely isnt as qualified as a real teacher). the student " \
                     "needs to interact with other kids their age and needs to be exposed to more ideas."

    def test_db_integrity(self):
        results = sql_engine.execute(
            "select text from posts join texts using(dataset_id, text_id) limit 1").fetchall()
        assert(len(results) == 1)
        first_text = results[0][0]
        assert(TestDb.correct_string == first_text)

    def test_orm_integrity(self):
        post = sql_session.query(Post).limit(1).all()
        assert(len(post) == 1)
        post = post[0]
        assert(TestDb.correct_string == post.text)


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
