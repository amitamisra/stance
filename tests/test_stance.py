#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
test_stance
----------------------------------

Tests for `stance` module.
"""

import unittest

import file_utilities
from createFeatures import Features
from pmi import pmi_loading
from correct_feature_dicts import *


class TestStance(unittest.TestCase):
    def setUp(self):
        """
        Setup the input_file and dep_file to use the ones in the resources folder, load rowdicts, and make sure we can
        pass some basic tests required by other tests.
        :return:
        """
        self.input_file = file_utilities.load_resource("tests/test_feature_input.csv")
        self.dep_file = file_utilities.load_resource("tests/test_feature_input-tweets.txt.predict")
        self.rowdicts = file_utilities.readCsv(self.input_file, "utf-8", ",")

        # If we cannot tokenize, normalize, and parse correctly we cannot pass the other tests
        self.rowdicts_tokenized_normalized = self.rowdicts.copy()
        self._test_token_structure(self.rowdicts_tokenized_normalized)
        self._test_token_normalization(self.rowdicts_tokenized_normalized)
        self._test_parse_structure()

    def tearDown(self):
        pass

    def test_pmi_resource_loading(self):
        pmi_dict = pmi_loading.load_pmi_from_resources(topics={"abortion"}, orders={1})
        try:
            pmi_dict[("mother",)]["abortion"]
        except KeyError:
            self.fail("pmi csvs are missing")

    def print_dict(self, d, title):
        """
        Debugging tool
        :param d:
        :param title:
        :return:
        """
        print(title)
        for k, v in d.items():
            print("\t\"{0}\": {1}".format(k, v))

    def _test_token_structure(self, rowdicts):
        correct_tokens = [
            [{'original_token': '.', 'pos': ',', 'token_index': 0}, {'original_token': '@heyrendelectric', 'pos': '@', 'token_index': 1}, {'original_token': '@darkkingzoro', 'pos': '@', 'token_index': 2}, {'original_token': 'and', 'pos': '&', 'token_index': 3}, {'original_token': "don't", 'pos': 'V', 'token_index': 4}, {'original_token': 'you', 'pos': 'O', 'token_index': 5}, {'original_token': 'think', 'pos': 'V', 'token_index': 6}, {'original_token': 'that', 'pos': 'O', 'token_index': 7}, {'original_token': 'would', 'pos': 'V', 'token_index': 8}, {'original_token': 'include', 'pos': 'V', 'token_index': 9}, {'original_token': 'respecting', 'pos': 'V', 'token_index': 10}, {'original_token': 'a', 'pos': 'D', 'token_index': 11}, {'original_token': "mother's", 'pos': 'S', 'token_index': 12}, {'original_token': 'life', 'pos': 'N', 'token_index': 13}, {'original_token': '?', 'pos': ',', 'token_index': 14}, {'original_token': '#prochoice', 'pos': '#', 'token_index': 15}, {'original_token': '#womenshealth', 'pos': '#', 'token_index': 16}],
            [{'token_index': 0, 'original_token': 'success', 'pos': 'N'}, {'token_index': 1, 'original_token': 'is', 'pos': 'V'}, {'token_index': 2, 'original_token': 'measured', 'pos': 'V'}, {'token_index': 3, 'original_token': 'by', 'pos': 'P'}, {'token_index': 4, 'original_token': 'the', 'pos': 'D'}, {'token_index': 5, 'original_token': 'quality', 'pos': 'N'}, {'token_index': 6, 'original_token': 'of', 'pos': 'P'}, {'token_index': 7, 'original_token': 'the', 'pos': 'D'}, {'token_index': 8, 'original_token': 'state', 'pos': 'N'}, {'token_index': 9, 'original_token': 'of', 'pos': 'P'}, {'token_index': 10, 'original_token': 'mind', 'pos': 'N'}, {'token_index': 11, 'original_token': '.', 'pos': ','}, {'token_index': 12, 'original_token': '#spirituality', 'pos': '#'}, {'token_index': 13, 'original_token': '#nibbana', 'pos': '#'}],
            [{'token_index': 0, 'original_token': 'be', 'pos': 'V'}, {'token_index': 1, 'original_token': 'thankful', 'pos': 'A'}, {'token_index': 2, 'original_token': 'for', 'pos': 'P'}, {'token_index': 3, 'original_token': 'every', 'pos': 'D'}, {'token_index': 4, 'original_token': 'person', 'pos': 'N'}, {'token_index': 5, 'original_token': 'who', 'pos': 'O'}, {'token_index': 6, 'original_token': 'apparently', 'pos': 'R'}, {'token_index': 7, 'original_token': 'puts', 'pos': 'V'}, {'token_index': 8, 'original_token': 'their', 'pos': 'D'}, {'token_index': 9, 'original_token': 'life', 'pos': 'N'}, {'token_index': 10, 'original_token': 'on', 'pos': 'P'}, {'token_index': 11, 'original_token': 'the', 'pos': 'D'}, {'token_index': 12, 'original_token': 'line', 'pos': 'N'}, {'token_index': 13, 'original_token': 'for', 'pos': 'P'}, {'token_index': 14, 'original_token': 'the', 'pos': 'D'}, {'token_index': 15, 'original_token': 'reproductive', 'pos': 'A'}, {'token_index': 16, 'original_token': 'health', 'pos': 'N'}, {'token_index': 17, 'original_token': 'of', 'pos': 'P'}, {'token_index': 18, 'original_token': 'men', 'pos': 'N'}, {'token_index': 19, 'original_token': 'and', 'pos': '&'}, {'token_index': 20, 'original_token': 'women', 'pos': 'N'}, {'token_index': 21, 'original_token': 'alike', 'pos': 'R'}, {'token_index': 22, 'original_token': '.', 'pos': ','}, {'token_index': 23, 'original_token': '#istandwithpp', 'pos': '#'}]
        ]
        Features.add_parses(rowdicts, "Tweet")
        for tokens, correct_tokens in zip(map(lambda d: d["tokens"], rowdicts), correct_tokens):
            for token, correct_token in zip(tokens, correct_tokens):
                self.assertTrue(token == correct_token)

    def _test_parse_structure(self):
        correct_parses = [
            {1: {'pos': '_', 'relation': '_', 'form': '.', 'head': -1}, 2: {'pos': '_', 'relation': '_', 'form': '@heyrendelectric', 'head': -1}, 3: {'pos': '_', 'relation': '_', 'form': '@darkkingzoro', 'head': -1}, 4: {'pos': '_', 'relation': '_', 'form': 'and', 'head': 0}, 5: {'pos': '_', 'relation': 'CONJ', 'form': "don't", 'head': 4}, 6: {'pos': '_', 'relation': '_', 'form': 'you', 'head': 5}, 7: {'pos': '_', 'relation': '_', 'form': 'think', 'head': 5}, 8: {'pos': '_', 'relation': '_', 'form': 'that', 'head': 9}, 9: {'pos': '_', 'relation': '_', 'form': 'would', 'head': 7}, 10: {'pos': '_', 'relation': '_', 'form': 'include', 'head': 9}, 11: {'pos': '_', 'relation': '_', 'form': 'respecting', 'head': 10}, 12: {'pos': '_', 'relation': '_', 'form': 'a', 'head': 14}, 13: {'pos': '_', 'relation': '_', 'form': "mother's", 'head': 14}, 14: {'pos': '_', 'relation': '_', 'form': 'life', 'head': 11}, 15: {'pos': '_', 'relation': '_', 'form': '?', 'head': -1}, 16: {'pos': '_', 'relation': '_', 'form': '#prochoice', 'head': -1}, 17: {'pos': '_', 'relation': '_', 'form': '#womenshealth', 'head': -1}},
            {1: {'pos': '_', 'relation': '_', 'form': 'success', 'head': 2}, 2: {'pos': '_', 'relation': '_', 'form': 'is', 'head': 0}, 3: {'pos': '_', 'relation': '_', 'form': 'measured', 'head': 2}, 4: {'pos': '_', 'relation': '_', 'form': 'by', 'head': 3}, 5: {'pos': '_', 'relation': '_', 'form': 'the', 'head': 6}, 6: {'pos': '_', 'relation': '_', 'form': 'quality', 'head': 4}, 7: {'pos': '_', 'relation': '_', 'form': 'of', 'head': 6}, 8: {'pos': '_', 'relation': '_', 'form': 'the', 'head': 9}, 9: {'pos': '_', 'relation': '_', 'form': 'state', 'head': 7}, 10: {'pos': '_', 'relation': '_', 'form': 'of', 'head': 9}, 11: {'pos': '_', 'relation': '_', 'form': 'mind', 'head': 10}, 12: {'pos': '_', 'relation': '_', 'form': '.', 'head': -1}, 13: {'pos': '_', 'relation': '_', 'form': '#spirituality', 'head': -1}, 14: {'pos': '_', 'relation': '_', 'form': '#nibbana', 'head': -1}},
            {1: {'pos': '_', 'relation': '_', 'form': 'be', 'head': 0}, 2: {'pos': '_', 'relation': '_', 'form': 'thankful', 'head': 1}, 3: {'pos': '_', 'relation': '_', 'form': 'for', 'head': 2}, 4: {'pos': '_', 'relation': '_', 'form': 'every', 'head': 5}, 5: {'pos': '_', 'relation': '_', 'form': 'person', 'head': 3}, 6: {'pos': '_', 'relation': '_', 'form': 'who', 'head': 8}, 7: {'pos': '_', 'relation': '_', 'form': 'apparently', 'head': 8}, 8: {'pos': '_', 'relation': '_', 'form': 'puts', 'head': 0}, 9: {'pos': '_', 'relation': '_', 'form': 'their', 'head': 10}, 10: {'pos': '_', 'relation': '_', 'form': 'life', 'head': 8}, 11: {'pos': '_', 'relation': '_', 'form': 'on', 'head': 8}, 12: {'pos': '_', 'relation': '_', 'form': 'the', 'head': 13}, 13: {'pos': '_', 'relation': '_', 'form': 'line', 'head': 11}, 14: {'pos': '_', 'relation': '_', 'form': 'for', 'head': 8}, 15: {'pos': '_', 'relation': '_', 'form': 'the', 'head': 17}, 16: {'pos': '_', 'relation': '_', 'form': 'reproductive', 'head': 17}, 17: {'pos': '_', 'relation': '_', 'form': 'health', 'head': 14}, 18: {'pos': '_', 'relation': '_', 'form': 'of', 'head': 17}, 19: {'pos': '_', 'relation': 'CONJ', 'form': 'men', 'head': 20}, 20: {'pos': '_', 'relation': '_', 'form': 'and', 'head': 8}, 21: {'pos': '_', 'relation': 'CONJ', 'form': 'women', 'head': 20}, 22: {'pos': '_', 'relation': '_', 'form': 'alike', 'head': 20}, 23: {'pos': '_', 'relation': '_', 'form': '.', 'head': -1}, 24: {'pos': '_', 'relation': '_', 'form': '#istandwithpp', 'head': -1}}
        ]
        rowdicts = self.rowdicts
        Features.add_dependencies(rowdicts, self.dep_file)
        for parse, correct_parse in zip(map(lambda d: d["tweebo_parse"], rowdicts), correct_parses):
            self.assertTrue(parse == correct_parse)

    def _test_token_normalization(self, rowdicts):
        correct_normalizations = [
            [{'pos': ',', 'corrected_token': '.', 'allCaps': False, 'token_index': 0, 'original_token': '.', 'repeatedOccurence': False}, {'pos': '@', 'corrected_token': 'user_ref', 'allCaps': False, 'token_index': 1, 'original_token': '@heyrendelectric', 'repeatedOccurence': False}, {'pos': '@', 'corrected_token': 'user_ref', 'allCaps': False, 'token_index': 2, 'original_token': '@darkkingzoro', 'repeatedOccurence': False}, {'pos': '&', 'corrected_token': 'and', 'allCaps': False, 'token_index': 3, 'original_token': 'and', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': "don't", 'allCaps': False, 'token_index': 4, 'original_token': "don't", 'repeatedOccurence': False}, {'pos': 'O', 'corrected_token': 'you', 'allCaps': False, 'token_index': 5, 'original_token': 'you', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': 'think', 'allCaps': False, 'token_index': 6, 'original_token': 'think', 'repeatedOccurence': False}, {'pos': 'O', 'corrected_token': 'that', 'allCaps': False, 'token_index': 7, 'original_token': 'that', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': 'would', 'allCaps': False, 'token_index': 8, 'original_token': 'would', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': 'include', 'allCaps': False, 'token_index': 9, 'original_token': 'include', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': 'respecting', 'allCaps': False, 'token_index': 10, 'original_token': 'respecting', 'repeatedOccurence': False}, {'pos': 'D', 'corrected_token': 'a', 'allCaps': False, 'token_index': 11, 'original_token': 'a', 'repeatedOccurence': False}, {'pos': 'S', 'corrected_token': "mother's", 'allCaps': False, 'token_index': 12, 'original_token': "mother's", 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'life', 'allCaps': False, 'token_index': 13, 'original_token': 'life', 'repeatedOccurence': False}, {'pos': ',', 'corrected_token': '?', 'allCaps': False, 'token_index': 14, 'original_token': '?', 'repeatedOccurence': False}, {'pos': '#', 'corrected_token': '#prochoice', 'allCaps': False, 'token_index': 15, 'original_token': '#prochoice', 'repeatedOccurence': False}, {'pos': '#', 'corrected_token': '#womenshealth', 'allCaps': False, 'token_index': 16, 'original_token': '#womenshealth', 'repeatedOccurence': False}],
            [{'pos': 'N', 'corrected_token': 'success', 'allCaps': False, 'token_index': 0, 'original_token': 'success', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': 'is', 'allCaps': False, 'token_index': 1, 'original_token': 'is', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': 'measured', 'allCaps': False, 'token_index': 2, 'original_token': 'measured', 'repeatedOccurence': False}, {'pos': 'P', 'corrected_token': 'by', 'allCaps': False, 'token_index': 3, 'original_token': 'by', 'repeatedOccurence': False}, {'pos': 'D', 'corrected_token': 'the', 'allCaps': False, 'token_index': 4, 'original_token': 'the', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'quality', 'allCaps': False, 'token_index': 5, 'original_token': 'quality', 'repeatedOccurence': False}, {'pos': 'P', 'corrected_token': 'of', 'allCaps': False, 'token_index': 6, 'original_token': 'of', 'repeatedOccurence': False}, {'pos': 'D', 'corrected_token': 'the', 'allCaps': False, 'token_index': 7, 'original_token': 'the', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'state', 'allCaps': False, 'token_index': 8, 'original_token': 'state', 'repeatedOccurence': False}, {'pos': 'P', 'corrected_token': 'of', 'allCaps': False, 'token_index': 9, 'original_token': 'of', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'mind', 'allCaps': False, 'token_index': 10, 'original_token': 'mind', 'repeatedOccurence': False}, {'pos': ',', 'corrected_token': '.', 'allCaps': False, 'token_index': 11, 'original_token': '.', 'repeatedOccurence': False}, {'pos': '#', 'corrected_token': '#spirituality', 'allCaps': False, 'token_index': 12, 'original_token': '#spirituality', 'repeatedOccurence': False}, {'pos': '#', 'corrected_token': '#nibbana', 'allCaps': False, 'token_index': 13, 'original_token': '#nibbana', 'repeatedOccurence': False}],
            [{'pos': 'V', 'corrected_token': 'be', 'allCaps': False, 'token_index': 0, 'original_token': 'be', 'repeatedOccurence': False}, {'pos': 'A', 'corrected_token': 'thankful', 'allCaps': False, 'token_index': 1, 'original_token': 'thankful', 'repeatedOccurence': False}, {'pos': 'P', 'corrected_token': 'for', 'allCaps': False, 'token_index': 2, 'original_token': 'for', 'repeatedOccurence': False}, {'pos': 'D', 'corrected_token': 'every', 'allCaps': False, 'token_index': 3, 'original_token': 'every', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'person', 'allCaps': False, 'token_index': 4, 'original_token': 'person', 'repeatedOccurence': False}, {'pos': 'O', 'corrected_token': 'who', 'allCaps': False, 'token_index': 5, 'original_token': 'who', 'repeatedOccurence': False}, {'pos': 'R', 'corrected_token': 'apparently', 'allCaps': False, 'token_index': 6, 'original_token': 'apparently', 'repeatedOccurence': False}, {'pos': 'V', 'corrected_token': 'puts', 'allCaps': False, 'token_index': 7, 'original_token': 'puts', 'repeatedOccurence': False}, {'pos': 'D', 'corrected_token': 'their', 'allCaps': False, 'token_index': 8, 'original_token': 'their', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'life', 'allCaps': False, 'token_index': 9, 'original_token': 'life', 'repeatedOccurence': False}, {'pos': 'P', 'corrected_token': 'on', 'allCaps': False, 'token_index': 10, 'original_token': 'on', 'repeatedOccurence': False}, {'pos': 'D', 'corrected_token': 'the', 'allCaps': False, 'token_index': 11, 'original_token': 'the', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'line', 'allCaps': False, 'token_index': 12, 'original_token': 'line', 'repeatedOccurence': False}, {'pos': 'P', 'corrected_token': 'for', 'allCaps': False, 'token_index': 13, 'original_token': 'for', 'repeatedOccurence': False}, {'pos': 'D', 'corrected_token': 'the', 'allCaps': False, 'token_index': 14, 'original_token': 'the', 'repeatedOccurence': False}, {'pos': 'A', 'corrected_token': 'reproductive', 'allCaps': False, 'token_index': 15, 'original_token': 'reproductive', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'health', 'allCaps': False, 'token_index': 16, 'original_token': 'health', 'repeatedOccurence': False}, {'pos': 'P', 'corrected_token': 'of', 'allCaps': False, 'token_index': 17, 'original_token': 'of', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'men', 'allCaps': False, 'token_index': 18, 'original_token': 'men', 'repeatedOccurence': False}, {'pos': '&', 'corrected_token': 'and', 'allCaps': False, 'token_index': 19, 'original_token': 'and', 'repeatedOccurence': False}, {'pos': 'N', 'corrected_token': 'women', 'allCaps': False, 'token_index': 20, 'original_token': 'women', 'repeatedOccurence': False}, {'pos': 'R', 'corrected_token': 'alike', 'allCaps': False, 'token_index': 21, 'original_token': 'alike', 'repeatedOccurence': False}, {'pos': ',', 'corrected_token': '.', 'allCaps': False, 'token_index': 22, 'original_token': '.', 'repeatedOccurence': False}, {'pos': '#', 'corrected_token': '#istandwithpp', 'allCaps': False, 'token_index': 23, 'original_token': '#istandwithpp', 'repeatedOccurence': False}]
        ]
        for rd, correctly_normalized_tokens in zip(rowdicts, correct_normalizations):
            Features.normalize_tokens(rd, Features.slang)
            self.assertTrue(rd["tokens"] == correctly_normalized_tokens)

    def get_features(self, feature_list, dependency_types=("general", "liwc", "sentiment_combined")):
        """
        Create a new Features object with the specified options and get the test features using it.
        :param feature_list: The features to use
        :param dependency_types: The dependency types to use if using the "dependency" feature
        :return: A list of feature dicts
        """
        feature_creator = Features(self.input_file, "", "Tweet", "Stance", "train", False, True, 5, "target",
                                   feature_list, "", "target", 2, self.dep_file, dependency_types, "abortion")
        return feature_creator.createFeaturesAllRows(self.rowdicts)

    def same_dict(self, d1, d2):
        """
        Check that two dict-like objects are the same. We could use d1 == d2, but I don't entirely trust this when
        comparing a defaultdict to a dict, and this method can be used for debugging if tests start failing.
        :param d1:
        :param d2:
        :return:
        """
        k1 = d1.keys()
        k2 = d2.keys()
        if len(k1) != len(k2):
            return False
        for k in k1:
            if k not in k2:
                return False
            if d1[k] != d2[k]:
                return False

        return True

    def _debug_failure(self, feature_dict, correct_dict):
        missing_keys = [key for key in correct_dict.keys() if key not in feature_dict.keys()]
        if len(missing_keys) > 0:
            print("The following keys were missing from the feature dictionary:")
            print("\t{0}".format(missing_keys))

        additional_keys = [key for key in feature_dict.keys() if key not in correct_dict]
        if len(additional_keys) > 0:
            print("Feature dictionary has additional keys:")
            print("\t{0}".format(additional_keys))

        incorrect_keys = [k for k, v in correct_dict.items() if k in feature_dict and feature_dict[k] != correct_dict[k]]
        if len(incorrect_keys) > 0:
            print("Incorrect feature values:")
            for incorrect_key in incorrect_keys:
                print("\t{key}: val={val}, correct={correct}".format(key=incorrect_key, val=feature_dict[incorrect_key],
                                                                     correct=correct_dict[incorrect_key]))

    def _test_feature(self, correct_feature_dicts, *get_feature_options):
        """
        Test a specific feature
        :param correct_feature_dicts: The correct feature dictionary, defined in correct_feature_dicts.py
        :param get_feature_options: the arguments for get_features, e.g. ["unigram"] or ["dependency"], ["general"]
        :return:
        """
        feature_dicts = self.get_features(*get_feature_options)
        self.assertTrue(len(feature_dicts) == len(correct_feature_dicts))
        for feature_dict, correct_dict in zip(feature_dicts, correct_feature_dicts):
            # Pop stance labels as they are not related to feature extraction
            if "Stance" in feature_dict:
                feature_dict.pop("Stance")
            if "Stance" in correct_dict:
                correct_dict.pop("Stance")

            self.assertTrue(self.same_dict(feature_dict, correct_dict), msg=self._debug_failure(
                feature_dict, correct_dict))

    def test_unigrams(self):
        self._test_feature(CORRECT_UNIGRAMS, ["unigram"])

    def test_bigrams(self):
        self._test_feature(CORRECT_BIGRAMS, ["bigram"])

    def test_trigrams(self):
        self._test_feature(CORRECT_TRIGRAMS, ["trigram"])

    def test_pos_bigrams(self):
        self._test_feature(CORRECT_POS_BIGRAMS, ["POS_bigram"])

    def test_pos_trigrams(self):
        self._test_feature(CORRECT_POS_TRIGRAMS, ["POS_trigram"])

    def test_liwc_counts(self):
        self._test_feature(CORRECT_LIWC_COUNTS, ["LIWCCount"])

    def test_high_pmi_ngram_counts(self):
        self._test_feature(CORRECT_HIGH_PMI_NGRAM_COUNTS, ["high_pmi_ngram_count"])

    def test_general_deps(self):
        self._test_feature(CORRECT_GENERAL_DEPS, ["dependency"], ["general"])

    def test_liwc_deps(self):
        self._test_feature(CORRECT_LIWC_DEPS, ["dependency"], ["liwc"])

    def test_sentiment_deps(self):
        self._test_feature(CORRECT_SENTIMENT_COMBINED_DEPS, ["dependency"], ["sentiment_combined"])

    def test_AFINN_sentiment(self):
        self._test_feature(CORRECT_AFINN_SENTIMENTS, ["AFINNSentiment"])

    def test_bing_lui_sentiment(self):
        self._test_feature(CORRECT_BING_LUI_SENTIMENTS, ["BingLuiSentiment"])


if __name__ == '__main__':
    import sys
    sys.exit(unittest.main())
