============
Installation
============

At the command line::

    $ easy_install stance

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv stance
    $ pip install stance
