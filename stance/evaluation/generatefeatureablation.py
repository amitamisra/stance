'''
Created on Feb 24, 2016

@author: amita
'''
import configparser
from stance.file_utilities import get_config_file
from stance.file_utilities import readTextFile
from stance.file_utilities import readCsv
import pandas as pd
from stance.evaluation.featureablation import Featureablation
from stance.evaluation.wekamodel  import WekaClassification
def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read(get_config_file('generatefeatureablation.ini'))
    inputdata=config.get(section,"csv_with_file_list")
    wekapath=config.get(section,"wekapath")
    featureabresults=config.get(section,"featureaboutput")
    arguments=(inputdata,wekapath,featureabresults)
    return arguments

def getscore(relevant_res,scoresdict,suffix):
   
    avgkey="average_"
    sumscores=0
    count=0
    for line in relevant_res:
        if "TP Rate" in line:
            index=count
            break
        else:
            count=count+1
                
    for indexscore in range(index+1,index+3):   
            # Take the f-measure
            line=relevant_res[indexscore]
            key=line.strip().split()[8]+"_"+suffix
            scoresdict[key]=float(line.strip().split()[4])
            sumscores = sumscores + float(line.strip().split()[4])
            avgkey=avgkey + key
    scoresdict[avgkey+"_"+suffix]=sumscores/2.0
    line=  relevant_res[index+3]      
    scoresdict[line.strip().split()[8]+"_"+suffix]=float(line.strip().split()[4])   

def writescores(outputtrain,outputtest,outputarff_CV,outputarff_predict,scoredict):
    if outputarff_CV:
        CVresults=readTextFile(outputarff_CV)
        relevant_res_cv= CVresults.index("=== Stratified cross-validation ===\n")
        getscore(CVresults[relevant_res_cv:],scoredict,"CV")
    if   outputarff_predict: 
        predict_results=readTextFile(outputarff_predict)
        relevant_res_p= predict_results.index("=== Error on test data ===\n")
        getscore(predict_results[relevant_res_p:],scoredict,"predict")
    scoredict["train"]= outputtrain
    scoredict["test"]=outputtest   
        
          
        
    
    

def execute(inputdata,wekajarpath,featureabresults):
    allrows=[]
    #inrows=readCsv(inputdata,"utf-8",",")
    inputdata_df=pd.read_csv(inputdata,na_filter=False)
    for index, row in inputdata_df.iterrows():
        inputtrain=row["inputTrain"]
        inputtest=row["inputTest"]
        outputtrain=row["outputTrain"]
        outputtest=row["outputTest"]
        keepattributecommand=row["keepattributecommand"]
        removeattributecommand=row["removeattributecommand"]
        reg_type_list=row["reg_type_list"]
        class_label=row["class_label"]
        folds=row["folds"]
        classifier=row["classifier"]
        classifier_option=row["classifier_option"]
        classifier_dirname=row["classifier_dirname"]
        choice=row["choice"]
        featureablation = Featureablation(inputtrain,inputtest,outputtrain,outputtest,keepattributecommand, removeattributecommand)
        if choice=="keep":
            scoredict=dict()
            featureablation.keepattribute(wekajarpath)
            classifier_obj= WekaClassification(reg_type_list,outputtrain,outputtest,class_label,folds,classifier,classifier_option,wekajarpath,classifier_dirname)
            classifier_obj.run()
            writescores(outputtrain,outputtest,classifier_obj.outputarff_CV,classifier_obj.outputarff_predict,scoredict)
            scoredict["classifier"]=classifier+classifier_option
            allrows.append(scoredict)
    df=pd.DataFrame(allrows)
    df.to_csv(featureabresults,index=False)
        
def run(section):
    args=readCongigFile(section)
    execute(args[0],args[1],args[2])
    
if __name__ == '__main__':
    section="ablation"
    run(section)
    section="ablationNohashtag"
    #run(section)
    
