'''
Created on Feb 19, 2016
Create a weka classifier to run from command line.
IMPORTANT: command line requires the column number of class, I am using last column by default. To use this 
class MOVE THE TRUE LABEL TO LAST COLUMN
@author: amita
'''

import os,sys, subprocess,pandas as pd
class WekaClassification():

    def __init__(self, reg_type_list,train_file, test_file, class_label,folds,classifier,classifier_option,wekapath,classifier_dir):
        
        self.regtype_list= reg_type_list
        self.train_file=train_file
        self.test_file=test_file
        self.class_label=class_label
        self.folds=folds
        self.wekapath=wekapath
        self.classifier_option=classifier_option
        self.classifier=classifier
        self.classifier_dir=classifier_dir
        self.outputarff_CV=""
        self.outputarff_predict=""
    
    def movelablecol_lastcol(self):
        df=pd.read_csv(self.train_file)
        cols = df.columns.tolist()
        lastindex=len(cols)
        cols.insert(lastindex-1, cols.pop(cols.index(self.reg_label)))
        df = df.reindex(columns= cols)
        df.to_csv(self.train_file,index=False)
        
        
    def run(self):
        #self.movelablecol_lastcol()
        if "CV" in self.regtype_list:
            self.execute_CV()
        if "predict" in self.regtype_list:
            self.execute_Predict() 
        if "test" in   self.regtype_list:
            self.execute_Predict()   
 
    # USE IT FOR CROSS VALIDATION
    def execute_CV(self):
        dirname=os.path.join(os.path.dirname(self.train_file), self.classifier_dir)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        outputfile=os.path.join(dirname, os.path.basename(self.train_file)[:-5]+"_CV.arff")
        cmd= str("java -cp " + self.wekapath+ " " + self.classifier + " -t " + self.train_file + " -c last " + " -x "+ str(self.folds) + " " + self.classifier_option +" > "+ outputfile)
        print(cmd)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        if proc.returncode != 0:
            print("weka Failed {0} {1} {2".format( proc.returncode, output,errors))
            sys.exit(-1)
        with open(outputfile, "a") as myfile:
            myfile.write("\ntrain File "+ self.train_file)
            myfile.write("\nclassifier "+ cmd)    
        self.outputarff_CV=outputfile 
        
            
    # FOR TEST SET    
    def execute_Predict(self):
        dirname=os.path.join(os.path.dirname(self.test_file), self.classifier_dir)
        if not os.path.exists(dirname):
            os.mkdir(dirname)
        outputfile=os.path.join(dirname, os.path.basename(self.test_file)[:-5]+"_predict.arff")
        cmd= str( "java -cp " + self.wekapath+ " " +self.classifier + " -t " + self.train_file +  " -T " + self.test_file+  " -c last " + " -x "+ str(self.folds) + " " + self.classifier_option +" > "+ outputfile)
        print(cmd)
        proc=subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        output, errors=proc.communicate()
        if proc.returncode != 0:
            print("weka Failed {0} {1} {2}".format( proc.returncode, output,errors))
            sys.exit(-1)
        with open(outputfile, "a") as myfile:
            myfile.write("\ntrain File "+ self.train_file)
            myfile.write("\ntest File"+ self.test_file)
            myfile.write("\nclassifier "+ cmd)
        self.outputarff_predict= outputfile    
        