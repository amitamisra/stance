"""
Take the 5 input csvs (one for each topic) and the 5 corresponding weka prediction files, add the predicted label to
each input row, and then write one file that has the predicted label for each input instance.

Use the "-p 0" option when applying your weka classifier to unseen data to get the correct output format for
predictions, and then redirect the output to a file.
"""
import configparser
import re, pandas as pd, os

from stance import file_utilities
from stance.file_utilities import get_config_file, readCsv


# Regex to parse weka prediction results
PREDICTION_RE = re.compile(r"[0-9]+\W+[0-9A-Za-z:]+\W+[0-9]:(?P<predicted_label>[A-Za-z]+)(?:\W+[0-9\.]+$)|(?:\W+\+\W+[0-9\.]+$)")
# Order defined for output file
DEFINED_ORDER =["ID", "Target", "Tweet", "Stance"]


def parse_predictions(prediction_rows):
    predicted_labels = list()
    for row in prediction_rows:
        row = row.strip()
        match = PREDICTION_RE.match(row)
        if match is not None:
            predicted_labels.append(match.group("predicted_label"))

    return predicted_labels


def row_dict_to_tab_line(row_dict):
    return "{ID}\t{Target}\t{Tweet}\t{Stance}\n".format(ID=row_dict["ID"], Target=row_dict["Target"], Tweet=row_dict["Tweet"], Stance=row_dict["Stance"])


def write_output(output_file, rows_with_predictions):
    with open(output_file, 'w', encoding="utf-8") as fh:
        fh.write("\t".join(DEFINED_ORDER) + "\n")
        fh.writelines(map(row_dict_to_tab_line, rows_with_predictions))


def  mergetruelabel(input_csvs):
    frames=[]
    for csv in input_csvs:
        df=pd.read_csv(csv, sep=",", encoding="utf-8")
        frames.append(df)
    res_dataframe=pd.concat(frames)
    rows_with_true_labels=res_dataframe.to_dict("records")
    return  rows_with_true_labels
   

def run():
    input_csvs, prediction_files, output_file = read_config()
    assert(len(input_csvs) == len(prediction_files))
    print("Combining input files with their prediction files.")
    print("MAKE SURE THESE ARE CORRECT COMBINATIONS:")
    for input_csv, prediction_file in zip(input_csvs, prediction_files):
        print("\t COMBINING {0}".format(input_csv))
        print("\t\t  WITH {0}".format(prediction_file))
    rows_with_predictions = list()
    for input_file, pred_file in zip(input_csvs, prediction_files):
        input_rows = readCsv(input_file, "utf-8", ",")
        predictions = parse_predictions(file_utilities.readTextFile(pred_file))
        assert(len(input_rows) == len(predictions))
        for input_row, prediction in zip(input_rows, predictions):
            input_row["Stance"] = prediction
            rows_with_predictions.append(input_row)

    # Sort the rows by their numerical ID because we may have changed the order when splitting into topics
    # and then recombining
    rows_with_true_labels=mergetruelabel(input_csvs)
    
    rows_with_predictions.sort(key=lambda row: int(row["ID"]))
    rows_with_true_labels.sort(key=lambda row: int(row["ID"]))
    output_truelabel=os.path.join(os.path.dirname(output_file),"actual_label.txt")
    write_output(output_file, rows_with_predictions)
    write_output(output_truelabel, rows_with_true_labels)


def read_config():
    config = configparser.ConfigParser()
    config.read(get_config_file("merge_predictions_config.ini"))
    input_csvs = config.get("stance", "input_csvs").split(",")
    prediction_files = config.get("stance", "prediction_files").split(",")
    output_file = config.get("stance", "output_file")
    return input_csvs, prediction_files, output_file


if __name__ == "__main__":
    run()




