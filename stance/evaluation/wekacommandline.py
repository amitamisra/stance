
'''
Created on Feb 19, 2016
weka
@author: amita

IMP Assumes predicted label to be last column
'''

from stance.evaluation.wekamodel  import WekaClassification
import configparser
from stance.file_utilities import get_config_file
def runwekaregression(section):
    args= readCongigFile(section)
    reg_type_list= args[0]
    classifier=args[1]
    classifier_option=args[2]
    classifier_dirname=args[3]
    train_file_list=args[4]
    test_file_list=args[5]
    class_label=args[6]
    folds=args[7]
    wekapath=args[8]
    execute(reg_type_list,classifier,classifier_option,classifier_dirname,train_file_list,test_file_list,class_label,folds,wekapath)

def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read(get_config_file('runweka_Config.ini'))
    reg_type_list = config.get(section, 'reg_type').split(",")
    classifier=config.get(section,'classifier')
    classifier_option=config.get(section,'classifier_option')
    classifier_dirname=config.get(section,'classifier_dirname')
    train_file_list= config.get(section, 'train_file_list').split(",")
    test_file_list= config.get(section, 'test_file_list').split(",")
    class_label=config.get(section, 'class_label')
    folds= int(config.get(section, 'folds'))
    wekapath=config.get(section,"wekapath")
    arguments=(reg_type_list,classifier,classifier_option,classifier_dirname,train_file_list,test_file_list,class_label,folds,wekapath)
    return  (arguments)


def execute(reg_type_list,classifier,classifier_option,classifier_dirname,train_file_list,test_file_list,class_label,folds,wekapath):
    train_test_tuple=zip(train_file_list,test_file_list)
    for train_test in  train_test_tuple:
        train_file= train_test[0]
        test_file= train_test[1]
        weka_regression= WekaClassification(reg_type_list,train_file, test_file, class_label,folds,classifier,classifier_option,wekapath,classifier_dirname)
        weka_regression.run()
    
    
if __name__ == '__main__':
    section="NB"
    runwekaregression(section)