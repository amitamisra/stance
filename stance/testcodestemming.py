'''
Created on Nov 6, 2015

@author: amita
'''
from nltk.stem.porter import *
from nltk.stem import WordNetLemmatizer
stemmer = PorterStemmer()
wordnet_lemmatizer = WordNetLemmatizer()
beforestem=["entertaining", "feminist", "feminism", "feminists"]
singles = [stemmer.stem(plural) for plural in beforestem]
print(singles)

lemmatize=[wordnet_lemmatizer.lemmatize(plural) for plural in beforestem]
print(lemmatize)
if __name__ == '__main__':
    pass