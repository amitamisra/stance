#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import json
import string

import norm_utilities
import re
from createFeatures import Features
from pmi.Pmi import Pmi
from collections import defaultdict
from db import *
from file_utilities import load_resource
from CMUTweetTagger import runtagger_parse

class NgramTopicPmiBuilder:

    def __init__(self, n: int, post_topics: list, tweet_topics: list, page_size=5000, report_at=1000, min_count=5, use_punctuation=False, topic_map=None):
        """
        :param n: The n in ngram
        :param post_topics: The list of topics to collect posts for
        :param tweet_topics: The list of tweets to collect posts for
        :param page_size: The amount of tweets to collect at once
        :param report_at: How many discussions/tweets to report after
        :param min_count: Minimum count of ngram occurences to be included in the pmi calculation
        :param use_punctuation: False to exclude punctuation, True to include
        :param topic_map: An optional dictionary that maps topic names in the database to topic names you want them to be stored as.
        :return:
        """
        self.use_punctuation = use_punctuation
        if not self.use_punctuation:
            self.punctuation = string.punctuation
        self.topic_id_to_topic = {t.topic_id: t.topic for t in sql_session.query(Topic).all()}
        self.topic_to_topic_id = {t.topic: t.topic_id for t in sql_session.query(Topic).all()}
        self.topic_map = {t.topic_id: t.topic for t in sql_session.query(Topic).all()}
        # Change topic to topic_id, might throw an error if the topic is not valid
        self.page_size = page_size
        self.report_at = report_at
        # Make a dictionary of pmi calculating objects, one for each value of n
        self.n = n
        self.min_count = min_count
        self.pmi = {n: Pmi(min_count=self.min_count) for n in range(1, self.n + 1)}
        self.post_topics = post_topics
        self.tweet_topics = tweet_topics
        self.topic_map = topic_map

        # slang dictionary for tweet normalization
        self.slang_dict = norm_utilities.build_slang_dict()

    def _get_topic(self, topic_id):
        return self.topic_id_to_topic[topic_id] if topic_id in self.topic_id_to_topic else None

    def _get_topic_id(self, topic):
        return self.topic_to_topic_id[topic] if topic in self.topic_to_topic_id else None

    def _retrieve_posts(self):
        """
        Retrieve texts from the database that are not in the other or unknown topics.
        :return: tuples of (topic, [posts])
        """
        query = sql_session.query(Discussion)
        # Build query for topics specified
        included_topic_ids = set(self._get_topic_id(topic) for topic in self.post_topics)
        excluded_topic_ids = [topic_id for topic_id in self.topic_id_to_topic.keys()
                              if topic_id not in included_topic_ids]

        for excl_topic_id in excluded_topic_ids:
            query = query.filter(Discussion.topic_id != excl_topic_id)

        disc_count = query.count()
        print("There are {0} discussions".format(disc_count))

        finished = 0
        next_report = self.report_at
        for disc in query:
            if finished == next_report:
                print("Finished {0}/{1} discussions".format(finished, disc_count))
                next_report += self.report_at

            yield self._get_topic(disc.topic_id), disc.get_posts()
            finished += 1

    def _retrieve_tweets(self):
        """
        Retrieve tweets from the database that are in one of the specified topics.
        :return: Tuples of (topic, [tweets])
        """
        query = sql_session.query(Tweet)
         # Build query for topics specified
        included_topic_ids = set(self._get_topic_id(topic) for topic in self.tweet_topics)
        excluded_topic_ids = [topic_id for topic_id in self.topic_id_to_topic.keys()
                              if topic_id not in included_topic_ids]

        # Need to fix this for tweets (maybe)
        for excl_topic_id in excluded_topic_ids:
            query = query.filter(Tweet.topic_id != excl_topic_id)

        tweet_count = query.count()
        print("There are {0} tweets".format(tweet_count))

        finished = 0
        next_report = self.report_at
        pages = tweet_count // self.page_size
        current_page = 0
        if tweet_count % self.page_size != 0:
            pages += 1
        while finished < tweet_count:
            if finished == next_report:
                print("Finished {0}/{1} tweets".format(finished, tweet_count))
                next_report += self.report_at
            finished += self.page_size
            yield query.slice(current_page * self.page_size, (current_page + 1) * self.page_size).all()
            current_page += 1

    def _get_ngrams(self, post) -> list:
        """
        Get ngrams from a post's text
        :param post:
        :return: A list of tuples that represent ngrams
        """
        if not post.text_obj.parse_data_loaded():
            post.text_obj.load_parse_data()

        words = [token["word"] for token in post.text_obj.tokens]
        if not self.use_punctuation:
            words = [word for word in words if word not in self.punctuation]
        result = defaultdict(list)
        for i in range(0, len(words)):
            n = self.n if i + self.n < len(words) else len(words) - i
            for j in range(1, n + 1):
                result[j].append(tuple(words[k].lower() for k in range(i, i + j)))

        return result

    def _get_ngrams_tweet(self, word_tokens) -> list:
        result = defaultdict(list)
        for i in range(0, len(word_tokens)):
            n = self.n if i + self.n < len(word_tokens) else len(word_tokens) - i
            for j in range(1, n + 1):
                result[j].append(tuple(word_tokens[k].lower() for k in range(i, i + j)))

        return result

    def _write_pmi(self, output_file: str):
        """
        Helper function to write the pmi for results to a csv.
        :param output_file:
        :return:
        """
        with open(output_file, 'w') as f_out:
            f_out.write("[")
            for i in range(1, len(self.pmi) + 1):
                for pmi_result in self.pmi[i].yield_pmi():
                    # Take apart the yielded pmi result. It is a tuple containing x, y, pmi, and npmi.
                    order = i
                    ngram = pmi_result[0]
                    topic = pmi_result[1]
                    # Remap topic name if it is in the specified topic_map
                    if topic_map is not None and topic in topic_map:
                        topic = topic_map[topic]
                    pmi = pmi_result[2]
                    npmi = pmi_result[3]
                    writable_dict = dict(order=i, ngram=ngram, topic=topic, pmi=pmi, npmi=npmi)
                    f_out.write(json.dumps(writable_dict) + ",")

            f_out.write("]")

    def build(self, output_file: str):
        """
        Build the pmi for the various options given to the constructor.
        :param output_file: Where to write a csv containing the pmi for ngrams
        :return:
        """
        for topic, posts in self._retrieve_posts():
            for post in posts:
                ngram_dict = self._get_ngrams(post)
                for n, ngrams in ngram_dict.items():
                    self.pmi[n].feed([(ngram, topic) for ngram in ngrams])

                del ngram_dict
                del post

        last_topic = None
        for tweet_page in self._retrieve_tweets():
            # Run the parser on just the text of the tweets
            tweet_dicts = [{"tweet_text": tweet.text_obj.text, "topic": self._get_topic(tweet.topic_id)}
                           for tweet in tweet_page]
            Features.add_parses(tweet_dicts, "tweet_text")
            for tweet_dict in tweet_dicts:
                Features.normalize_tokens(tweet_dict, Features.slang)
                topic = tweet_dict["topic"]
                if last_topic != topic:
                    last_topic = topic
                    print("tweet is on topic: {0}".format(topic))
                ngram_dict = self._get_ngrams_tweet([token["corrected_token"] for token in tweet_dict["tokens"]])
                for n, ngrams in ngram_dict.items():
                    self.pmi[n].feed([(ngram, topic) for ngram in ngrams])

        self._write_pmi(output_file)


if __name__ == "__main__":
    topic_map = dict()
    topic_map["climate change"] = "climate change is a real concern"
    topic_map["existence of god"] = "atheism"
    NgramTopicPmiBuilder(n=3,
                         page_size=1000,
                         report_at=5000,
                         min_count=5,
                         post_topics=["abortion", "climate change", "existence of god"],
                         tweet_topics=["atheism", "climate change is a real concern", "feminist movement",
                                       "Hillary Clinton", "abortion"],
                         topic_map=topic_map)\
        .build("/home/brian/git/stance/test.json")

