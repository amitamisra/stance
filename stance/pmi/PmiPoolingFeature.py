#!/usr/bin/env python
# -*- coding: utf-8 -*-
from math import floor

from collections import defaultdict
from pmi.PmiTopicTagger import PmiTopicTagger


class PmiPoolingFeatureExtractor:

    def __init__(self, top_n_percent: int, topics=["abortion", "climate change is a real concern", "atheism",
                                                   "Hillary Clinton", "feminist movement"], order=3, normalized=True):
        """
        :param top_n_percent: The top n percent of pmi words to use from each topic in order to generate the pmi pool
        :param order: max ngram order. Will use n = [1, 2,..., order]
        :param normalized: use normalized pmi
        :return:
        """
        self.tagger = PmiTopicTagger(orders=list(range(1, order + 1)), topics=topics)
        self.top_n_percent = top_n_percent
        self.order = order
        self.normalized = normalized

        # initialized in self._make_pmi_pool
        self.ngram_pool = set()
        self._make_pmi_pool()

    def _make_pmi_pool(self):
        # Split up pmi_dict by topic
        by_topic = defaultdict(list)
        for ngram, topic_dict in self.tagger.pmi_dict.items():
            for topic, pmi_tuple in topic_dict.items():
                by_topic[topic].append((ngram, pmi_tuple))

        # Sort each topic and put the top n percent from each into the pool
        for topic, pmi_list in by_topic.items():
            if self.normalized:
                pmi_list.sort(key=lambda ngram_pmi_tup: ngram_pmi_tup[1][1], reverse=True)
            else:
                pmi_list.sort(key=lambda ngram_pmi_tup: ngram_pmi_tup[1][0], reverse=True)

            for ngram_pmi_tup in pmi_list[:floor(len(pmi_list) * (self.top_n_percent / 100))]:
                self.ngram_pool.add(ngram_pmi_tup[0])

    def featurize(self, ngram_tuples: list) -> int:
        """
        :param ngram_tuples: A list of the ngram tuples as made by Features.ngrams_from_rowdicts
        :return: A count of the occurences of a high pmi ngram
        """
        count = 0
        for ngram in ngram_tuples:
            if ngram in self.ngram_pool:
                count += 1
        return count

if __name__ == "__main__":
    # Quick test
    feature = PmiPoolingFeatureExtractor(90)
    print(feature.featurize([dict(token="Abortion"), dict(token="is"), dict(token="wrong")]))

