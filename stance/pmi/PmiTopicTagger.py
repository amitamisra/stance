#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pmi.pmi_loading import load_pmi_from_resources


class PmiTopicTagger:

    def __init__(self, orders=(1,), topics=("abortion",)):
        """
        :param orders: The orders (n in ngrams) to load pmi ngrams for
        :param topics: The topics to load pmi ngrams for
        :return:
        """
        self.pmi_dict = load_pmi_from_resources(topics=topics, orders=orders)

    def tag_tuple(self, word_tuple: tuple, topic: str, normalized: bool) -> float:
        if word_tuple in self.pmi_dict:
            tuple_entry = self.pmi_dict[word_tuple]
            if topic in tuple_entry:
                pmi_entry = tuple_entry[topic]
                if normalized:
                    return pmi_entry[1]
                else:
                    return pmi_entry[0]

        return None

if __name__ == "__main__":
    # Executable main as a quick test
    print(PmiTopicTagger(orders=[1, 2]).tag_tuple(("termination", "of"), "abortion", normalized=True))
