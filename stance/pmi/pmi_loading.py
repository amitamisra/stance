#!/usr/bin/env python
# -*- coding: utf-8 -*-
import csv
import json

from collections import defaultdict
from file_utilities import load_resource


# May raise FileNotFoundError
pmi_location = load_resource("pmi/tweet_trigrams_bigrams_unigrams_by_topic.json")

def load_pmi_from_resources(topics: set, orders: set):
    """
    :param topics: A set of strings for the topics you want to load. Can load only specific topics to save memory.
        None to load all.
    :param orders: A set of ints for the ngram orders (the n in ngram) you want to include
        None to load all.
    :return: A dictionary where the keys are ngram tuples, and the values are dictionaries
    of topic -> (pmi, npmi)
    """
    if topics is not None:
        topics = set(topic.lower() for topic in topics)
    results = defaultdict(dict)
    with open(pmi_location, 'r') as f_in:
        pmi_dicts = json.load(f_in)
        for pmi_entry in pmi_dicts:
            if (topics is None or pmi_entry["topic"].lower() in topics) and (orders is None or pmi_entry["order"] in orders):
                ngram_tuple = tuple(word for word in pmi_entry["ngram"])
                results[ngram_tuple][pmi_entry["topic"].lower()] = (float(pmi_entry["pmi"]), float(pmi_entry["npmi"]))

    return results

def write_pmi_csv(outfile):
    pmi_dict = load_pmi_from_resources(None, None)
    with open(outfile, 'w', encoding='utf-8') as f_out:
        writer = csv.writer(f_out)
        writer.writerow(["ngram", "topic", "order", "pmi", "npmi"])
        for ngram_tuple, topic_dict in pmi_dict.items():
            for topic, pmi_tuple in topic_dict.items():
                writer.writerow([ngram_tuple, topic, len(ngram_tuple), pmi_tuple[0], pmi_tuple[1]])


