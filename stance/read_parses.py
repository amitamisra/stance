#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
''' Usage: Because we can't run this from a subprocess, there are a few steps:
    Requirements: Download and install the TweeboParser to resources folder. Python 2
    future package must be installed
    1. Make sure your virtual environment is not activated.
    2. Run the script
    3. Now add the dependency file to the respective option in createFeaturesConfig.ini
       The name of the dependency file is the same as the generated txt file except is has '.predict' appended to it.
    5. Activate your virtual environment and run createFeatures.py
'''

import os, re, sys
import os.path
import file_utilities
from collections import defaultdict
import subprocess
from configparser import ConfigParser
import codecs
from norm_utilities import ascii_only_unicodeerror


def write_tweet_file(filename):
    # rowdicts = file_utilities.readCsv(filename, "latin-1", ",")
    rowdicts = file_utilities.readCsv(filename, "utf-8", ",")
    basename = os.path.basename(filename)

    parentpath = os.path.dirname(os.getcwd())
    tweet_path = str(os.path.join(parentpath, "resources", "TweeboParser"))
    tweet_file = tweet_path + '/' + basename[:-4] + "-tweets.txt"

    # lines = [rowdict["Tweet"].decode("latin1").encode("utf-8") + "\n" for rowdict in rowdicts]
    lines = [rowdict["Tweet"] + "\n" for rowdict in rowdicts]
    file_utilities.writeTextFile(tweet_file, lines)


def run_parser(filename):
    write_tweet_file(filename)
    current_dir = os.getcwd()
    basename = os.path.basename(filename)

    parentpath = os.path.dirname(os.getcwd())
    parser_path = str(os.path.join(parentpath, "resources", "TweeboParser"))
    print(parser_path)
    tweet_file = basename[:-4] + "-tweets.txt"
    print(tweet_file)
    os.chdir(parser_path)
    subprocess.call("./run.sh " + tweet_file, shell=True)
    os.chdir(current_dir)
    print("CLOSED")


def split_file(filename):
    total_lines = ''
    for line in file_utilities.readTextFile(filename):
        total_lines += line
    tweets = total_lines.split("\n\n")

    tweet_parses = []
    for tweet in tweets:
        tweet_parses.append(tweet.split("\n"))
    return tweet_parses


def create_parse_dict(tweet_parse):
    parse = {}
    for token_parse in tweet_parse:
        values = token_parse.split('\t')
        parse[int(values[0])] = {"form": ascii_only_unicodeerror(values[1]), "pos": values[5],
                                 ascii_only_unicodeerror("head"): int(values[6]), "relation": values[7]}
    return parse


if __name__ == '__main__':
    config = ConfigParser()
    parentdir = os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir, 'docs', 'configFiles', 'createFeaturesConfig.ini'))
    inputFile = config.get('stance', 'inputFile')
    run_parser(inputFile)
