#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Nov 19, 2015

@author: amita
'''
from stance import file_utilities
import os
import configparser
import pandas 
import norm_utilities
import random
import re
import stanceNostancetwitter
from operator import itemgetter


def removeSemEvaltag(rows,textCol):
        for row in rows:
            text= row[textCol]
            semEvalhastag=str(text).find("#SemST")
            newtext=text[:semEvalhastag]
            row[textCol]=newtext
        return rows 

def changetextencoding(rowdicts,textfield):
    regex = re.compile(r'[\n\r\t]')
    for row in rowdicts:
        text=row[textfield].encode("utf-8") 
        text=text.decode("utf-8")
        text = regex.sub(' ',text)
        newtext=norm_utilities.ascii_only_unicodeerror(text)
        row[textfield]=newtext
    
def changelatin_to_Utf8tabtocomma(inputFile,Utf8Csv,textField):
    rowdicts=file_utilities.readCsv(inputFile, "latin-1", "\t")
    colnames=rowdicts[0].keys()
    changetextencoding(rowdicts,textField)
    file_utilities.writeCsv(Utf8Csv, rowdicts,colnames)


def changelatin_to_Utf8(inputCsv,outputCsv,textfield):
    rowdicts=file_utilities.readCsv(inputCsv, "latin-1", ',')
    changetextencoding(rowdicts,textfield)
    colnames=rowdicts[0].keys()
    file_utilities.writeCsv(outputCsv, rowdicts,colnames)


def createtopicFile(inputCsv,outputdir,targetList, targetCol): 
    rowstarget=[]
    allFiles=[]
    noOfTargets=len(targetList)
    rowdicts=file_utilities.readCsv(inputCsv, 'utf-8', ",")
    for count in range(0,noOfTargets):
        rows_withtarget=[row for row in rowdicts if row[targetCol]==targetList[count]]
        rowstarget.append(rows_withtarget)
        
    for count in range(0,noOfTargets):
        alltargetrows=rowstarget[count]
        dirpath=os.path.join(outputdir,alltargetrows[0][targetCol])
        os.mkdir(dirpath)
        filenametarget= os.path.join(dirpath, alltargetrows[0][targetCol]+".csv")
        file_utilities.writeCsv(filenametarget,alltargetrows, alltargetrows[0].keys())
        allFiles.append(filenametarget)

    return allFiles
# read semeval test data, change separator to comma, encoding to utf-8, remove semeval tag
def executetest(inputFile,semEvalTestUtf8Csv,outputDir,targetList, targetCol,textField):  
    changelatin_to_Utf8tabtocomma(inputFile,semEvalTestUtf8Csv,textField)
    rows=file_utilities.readCsv(semEvalTestUtf8Csv, "utf-8", ",")
    newrows=removeSemEvaltag(rows,textField)
    colnames=newrows[0].keys()
    file_utilities.writeCsv(semEvalTestUtf8Csv, newrows, colnames)
    allFiles=createtopicFile(semEvalTestUtf8Csv,outputDir,targetList, targetCol)
    createbalanceFilesforAll(allFiles)
    

def runpreprocesstest(section):
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    semEvalTest = config.get(section, 'semevalInputTrial')
    semEvalTestUtf8 = config.get(section, 'semevalTrialUtf8')
    outputDir= config.get(section, 'outputDir')
    targetList=list(config.get(section,'targetList').split(","))
    textField=config.get(section,'textfield')
    targetCol=config.get(section,'Target')
    executetest(semEvalTest,semEvalTestUtf8,outputDir,targetList,targetCol,textField)


#change encoding to utf-8
def executetwitter(inputCsv,outputCsv,textField): 
    #addheader(inputCsv)
    changelatin_to_Utf8(inputCsv,outputCsv,textField)
     

def runpreprocesstwitter(section) :
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    inputCsv=config.get(section, 'twitterInput')
    outputCsv=config.get(section, 'twitterUtf-8')
    textField=config.get(section,'textfield')
    executetwitter(inputCsv,outputCsv,textField) 
    
def addheader(inputcsv):
    df = pandas.read_csv(inputcsv)
    df.columns = ['ID','Target', 'Stance' ,'HashTag','Tweet']
    df.to_csv(inputcsv)
   
def runpreprocesstrain(section):
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    semEvalInputTrain = config.get(section, 'semevalInputtrain')
    semEvalTrainUtf8 = config.get(section, 'semevaltrainUtf8')
    outputDir= config.get(section, 'outputDir')
    targetList=list(config.get(section,'targetList').split(","))
    targetCol=config.get(section,'Target')
    textField=config.get(section,'textfield')
    executetrain(semEvalInputTrain,semEvalTrainUtf8,outputDir,targetList, targetCol,textField)
      
#read semeval train data, change separator to comma, encoding to utf-8, remove semeval tag 
def executetrain(inputTrain,trainUtf8Csv,outputDir,targetList, targetCol,textField):
    changelatin_to_Utf8tabtocomma(inputTrain,trainUtf8Csv,textField)
    rows=file_utilities.readCsv(trainUtf8Csv, "utf-8", ",")
    newrows=removeSemEvaltag(rows,textField)
    colnames=newrows[0].keys()
    file_utilities.writeCsv(trainUtf8Csv, newrows, colnames)
    allFiles=createtopicFile(trainUtf8Csv,outputDir,targetList, targetCol)
    createbalanceFilesforAll(allFiles)

    
def createbalanceFilesforAll(allFiles):
    random.seed(10)
    for filename in allFiles:
        againstlist=[]
        forlist=[]
        nonelist=[]
        Parentdirectory=os.path.dirname(filename)
        topic=os.path.basename(filename)[:-4]
        filename_suffix = '.csv'
        
        rowdicts=file_utilities.readCsv(filename, "utf-8", ",")
        for row in rowdicts:
            if row["Stance"].lower()=="favor":
                forlist.append(row)
            if row["Stance"].lower()=="against":
                againstlist.append(row)
            if row["Stance"].lower()=="none":
                nonelist.append(row)   
                     
        forcount=len(forlist)   
        againstcount=len(againstlist) 
        nonecount=len(nonelist)
          
        random.shuffle(againstlist)
        random.shuffle(forlist)
        random.shuffle(nonelist)
        
        if forcount <  againstcount:
            number=forcount
        else:
            number=againstcount 
               
        forrows=forlist[0:number]
        againstrows=againstlist[0:number]
        nonerows=nonelist[0:number]
        
        allthreelistList=forrows+againstrows+nonerows
        foragainstlist=forrows+againstrows
        
        if   2*number <  nonecount:
            stanceNostancecount= 2*number
        else:
            stanceNostancecount= nonecount
            
        for_againstcount=int(stanceNostancecount/2.0)
        stanceNoStanceList=nonelist[0:stanceNostancecount] + forlist[0:for_againstcount] + againstlist[0:for_againstcount]
        
        colnames=stanceNoStanceList[0].keys()
        
        base_filename="balStanceNoStance" + topic
        StanceNostanceFile=os.path.join(Parentdirectory, base_filename + filename_suffix)
        file_utilities.writeCsv(StanceNostanceFile, stanceNoStanceList, colnames)
        
        base_filename="balAllthree" + topic
        AllthreeFile=os.path.join(Parentdirectory, base_filename + filename_suffix)
        file_utilities.writeCsv(AllthreeFile, allthreelistList, colnames)
        
        base_filename="balForAgainst" + topic
        forgagainstFile=os.path.join(Parentdirectory, base_filename + filename_suffix)
        file_utilities.writeCsv(forgagainstFile, foragainstlist, colnames)

def runpreprocessPredict(section):
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    semevalInputPredict = config.get(section, 'semevalInputPredict')
    semEvalPredictUtf8 = config.get(section, 'semEvalPredictUtf8')
    outputPredictDir= config.get(section, 'outputPredictDir')
    targetList=list(config.get(section,'targetList').split(","))
    targetCol=config.get(section,'Target')
    textField=config.get(section,'textfield')
    executePredict(semevalInputPredict,semEvalPredictUtf8,outputPredictDir,targetList, targetCol,textField)



def executePredict(semevalInputPredict,semEvalPredictUtf8,outputPredictDir,targetList, targetCol,textField):
    changelatin_to_Utf8tabtocomma(semevalInputPredict,semEvalPredictUtf8,textField)
    rows=file_utilities.readCsv(semEvalPredictUtf8, "utf-8", ",")
    newrows=removeSemEvaltag(rows,textField)
    colnames=newrows[0].keys()
    file_utilities.writeCsv(semEvalPredictUtf8, newrows, colnames)
    createtopicFile(semEvalPredictUtf8,outputPredictDir,targetList, targetCol)


def runpreprocessaddaddpredict(section):
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    semevalInputOrig= config.get(section, 'InputOrig')
    semevalInputStance= config.get(section, 'InputStance')
    semevalOutput= config.get(section, 'Output')
    rowsInput=file_utilities.readCsv(semevalInputOrig, "utf-8", ",")
    rowsStance=file_utilities.readCsv(semevalInputStance,"utf-8", ",")
    
    rowssortedInput=sorted(rowsInput, key=itemgetter('ID')) 
    rowssortedStance=sorted(rowsStance,key=itemgetter('ID')) 
    for count in range(0,len(rowssortedInput)):
        rowssortedInput[count]["Stance"]=rowssortedStance[count]["Stance"]
    colnames= list(rowssortedInput[0].keys())   
    file_utilities.writeCsv(semevalOutput,rowssortedInput, colnames)    
    
    
    

def run(section): 
    if section=="train":
        runpreprocesstrain(section)
    if section=="test":
        runpreprocesstest(section)
    if section=="twitter":
        runpreprocesstwitter(section)
    if section=="predict":
        runpreprocessPredict(section)  
    if section=="twitterRandom":
        runrandom(section)
    if section=="mergeTopicsTwitter":
        mergealltopic(section)
    if section=="stanceNoStance":
        stanceNostancetwitter.run(section)  
    if section=="addpredicthillary":   
        runpreprocessaddaddpredict(section)  
    if section=="addpredictabortion":   
        runpreprocessaddaddpredict(section)   
    if section=="gold":
        runpreprocessPredict(section)          
        
          
def runrandom(section):
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    inputRandom= config.get(section, 'inputRandom')
    randomWithHeaders = config.get(section, 'randomWithHeaders')
    rowdicts= file_utilities.readCsv(inputRandom, "utf-8", ",")
    count=0
    allrows=[]
    for row in rowdicts:
        newrow=dict()
        count=count+1
        newrow["Stance"]="NONE"
        newrow["ID"]= count
        newrow["Tweet"]=row["Tweet"]
        newrow["Target"]="Random"
        newrow["HashTag"]="NONE"
        allrows.append(newrow)
    colnames=allrows[0].keys()    
    file_utilities.writeCsv(randomWithHeaders, allrows,colnames)  
    


def mergetopic(input1,input2,output):
    rowdicts1= file_utilities.readCsv(input1, "utf-8", ",")
    rowdicts2= file_utilities.readCsv(input2, "utf-8", ",")
    rowdicts=rowdicts1+rowdicts2
    colnames=rowdicts[0].keys()
    file_utilities.writeCsv(output, rowdicts, colnames)
    


def BalancetwitterProCon(topicfile):   
        random.seed(10)
        againstlist=[]
        forlist=[]
        Parentdirectory=os.path.dirname(topicfile)
        topic=os.path.basename(topicfile)[:-4]
        filename_suffix = '.csv'
        
        rowdicts=file_utilities.readCsv(topicfile, "utf-8", ",")
        for row in rowdicts:
            if row["Stance"].lower()=="favor":
                forlist.append(row)
            if row["Stance"].lower()=="against":
                againstlist.append(row)
        
        forcount=len(forlist)   
        againstcount=len(againstlist) 
          
        random.shuffle(againstlist)
        random.shuffle(forlist)
        
        if forcount <  againstcount:
            number=forcount
        else:
            number=againstcount 
               
        forrows=forlist[0:number]
        againstrows=againstlist[0:number]
        
        foragainstlist=forrows+againstrows
        base_filename="balForAgainst" + topic
        forgagainstFile=os.path.join(Parentdirectory, base_filename + filename_suffix)
        colnames=foragainstlist[0].keys()
        file_utilities.writeCsv(forgagainstFile, foragainstlist, colnames)
        
         
    
def mergealltopic(section): 
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    
    abortionAgainst=config.get(section, "abortionAgainst")
    abortionFavour=config.get(section, "abortionFavour")
    abortion=config.get(section, "abortion")
    
    atheismFavor=config.get(section,  "atheismFavor")
    atheismAgainst=config.get(section,  "atheismAgainst")
    atheism=config.get(section,  "atheism")
    
    climateFavor=config.get(section,"climateFavor" )
    climateAgainst=config.get(section, "climateAgainst")
    climate=config.get(section, "climate")
    
    feminismAgainst=config.get(section, "feminismAgainst")
    feminismFavor=config.get(section, "feminismFavor")
    feminism=config.get(section, "feminism")
    
    hillaryFavor=config.get(section,  "hillaryFavor")
    hillaryAgainst=config.get(section, "hillaryAgainst")
    hillary=config.get(section, "hillary")
    
    mergetopic(abortionAgainst,abortionFavour,abortion)
    mergetopic(atheismFavor,atheismAgainst,atheism)
    mergetopic(climateFavor,climateAgainst,climate)
    mergetopic(feminismAgainst,feminismFavor,feminism)
    mergetopic(hillaryFavor,hillaryAgainst,hillary)
    BalancetwitterProCon(abortion)
    BalancetwitterProCon(atheism)
    BalancetwitterProCon(feminism)
    BalancetwitterProCon(climate)
    BalancetwitterProCon(hillary)

    
    
if __name__ == '__main__':
    
    #section="twitterRandom" # done once, not needed now
    #section="twitter" do it for all topics
    #section="mergeTopicsTwitter"
    #run(section)
    #section="stanceNoStance"
    #section="predict"
    #section="addpredicthillary"
    #section="addpredictabortion"
    section="gold"
    run(section)
    
    
    
    