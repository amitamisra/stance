__author__ = 'brian'

import csv
import random
from collections import defaultdict

training = "/home/brian/git/stance/data/training/semeval2016-task6-trainingdata.csv"
trial = "/home/brian/git/stance/data/training/semeval2016-task6-trialdata.csv"
train_out = "/home/brian/git/stance/data/training/semeval2016-task6-trainingdata_balanced.csv"
dev_out = "/home/brian/git/stance/data/training/semeval2016-task6-devdata_balanced.csv"
train_percentage = 0.8

stance = defaultdict(list)

# Read data
with open(training, 'r') as tra_in, open(trial, 'r') as tri_in:
    tra_reader = csv.reader(tra_in)
    header = next(tra_reader)
    for row in tra_reader:
        stance[row[header.index("Stance")]].append(row)
    tri_reader = csv.reader(tri_in)
    next(tri_reader)
    for row in tri_reader:
        stance[row[header.index("Stance")]].append(row)

    stance["STANCE"] = stance["FAVOR"] + stance["AGAINST"]

# sample data into new files
smaller = len(stance["NONE"]) if len(stance["NONE"]) < len(stance["STANCE"]) else len(stance["STANCE"])
random.seed(100)

usable_none = random.sample(stance["NONE"], smaller)
usable_stance = random.sample(stance["STANCE"], smaller)

train_cutoff = round(smaller * train_percentage)
training_set = usable_none[:train_cutoff] + usable_stance[:train_cutoff]
dev_set = usable_none[train_cutoff:] + usable_stance[train_cutoff:]

with open(train_out, 'w') as tra_out, open(dev_out, 'w') as dev_out:
    writer = csv.writer(tra_out)
    writer.writerow(header)
    writer.writerows(training_set)
    writer = csv.writer(dev_out)
    writer.writerow(header)
    writer.writerows(dev_set)

