import os
import json
import sqlalchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker, reconstructor
from ._extras import root_dir

def _load_connection_details(filename=None):
    if filename is None:
        filename = os.path.abspath(os.path.join(root_dir, 'sql_auth.json'))
    connection_details_file = open(filename)
    connection_details_raw = json.load(connection_details_file)
    connection_details = connection_details_raw[connection_details_raw['default_configuration']]
    return connection_details


def _setup_connection(connection_details=None):
    if connection_details is None:
        try:
            connection_details = _load_connection_details()
        except FileNotFoundError:
            print("WARNING: Database connection not set up.")
            return None, None

    _connection_string = connection_details['dbapi']+'://'+connection_details['username']
    if connection_details['password'] != '':
        _connection_string += ':'+connection_details['password']
    _connection_string += '@'+connection_details['host']+':'+connection_details['port']+'/'+connection_details['database']

    sqlalchemy_engine = sqlalchemy.create_engine( _connection_string)
    Session = sessionmaker(bind=sqlalchemy_engine)
    session = Session()
    return sqlalchemy_engine, session

sql_engine, sql_session = _setup_connection()
if sql_engine is not None and sql_session is not None:
    sql_metadata = sqlalchemy.MetaData(bind=sql_engine)
else:
    sql_metadata = None
SQL_Base = declarative_base()
