# "from iac_objects import *"

# Note: Might want to use __all__ = ['...']

from ._sql_connect import sql_engine, sql_metadata, SQL_Base, sql_session
if sql_session is not None:
    from ._dataset import Dataset, load_dataset, create_dataset, get_dataset_id
    from ._discussion import Discussion
    from ._post import Post
    from ._tweet import Tweet
    from ._text import Text, load_text_obj
    from ._topic import Topic

    from ._extras import root_dir

    import sqlalchemy as sqlalchemy  # odd syntax, but makes it work
