import json
from collections import defaultdict

from ._sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata
from sqlalchemy.orm import relationship
import sqlalchemy


class Text(SQL_Base):
    __table__ = sqlalchemy.Table('texts', sql_metadata, autoload=True)
    markup_list = relationship("Markup")

    def __init__(self, dataset_id: int, text_id: int, text: str):
        self.dataset_id = dataset_id
        self.text_id = text_id
        self.text = text

    def load_parse_data(self) -> bool:
        if self.parse_data_loaded():
            return True

        self.tokens = get_tokens(self.dataset_id, self.text_id)
        self.parse = get_parse(self.dataset_id, self.text_id)
        self.dependencies = get_dependencies(self.dataset_id, self.text_id)
        self.named_entities = get_named_entities(self.dataset_id, self.text_id)
        self.corefs = get_corefs(self.dataset_id, self.text_id)
        self.sentences = get_sentences(self.dataset_id, self.text_id)
        self.link_tokens()

        return self.parse_data_loaded()

    def parse_data_loaded(self) -> bool:
        return hasattr(self, 'tokens') and len(self.tokens) > 0

    def link_tokens(self):
        for entry in self.parse:
            entry['token'] = self.tokens[entry['token_index']] if entry.get('token_index') is not None else None
            if entry['token']:
                entry['token']['parse_node'] = entry
        for entry in self.dependencies:
            entry['governor'] = self.tokens[entry['governor_token_index']] if entry.get('governor_token_index') is not None else None
            entry['dependent'] = self.tokens[entry['dependent_token_index']]
        for entry in self.named_entities:
            entry['tokens'] = self.tokens[entry['token_index_first']:entry['token_index_last']+1]
            entry['text'] = self.text[entry['tokens'][0]['start']:entry['tokens'][-1]['end']]
        for coref_list in self.corefs.values():
            for entry in coref_list:
                entry['tokens'] = self.tokens[entry['token_index_first']:entry['token_index_last']+1]
                entry['head_token'] = self.tokens[entry['token_index_head']]
                entry['text'] = self.text[entry['tokens'][0]['start']:entry['tokens'][-1]['end']]
        for sentence in self.sentences:
            sentence['tokens'] = [token for token in self.tokens if token['sentence_index'] == sentence['sentence_index']]
            sentence['text'] = self.text[sentence['start']:sentence['end']]


def get_tokens(dataset_id, text_id):
    query = """select *,
  (select word
    from words as lemma_words
    where lemma_words.word_id = lemma_word_id limit 1) as lemma
      from tokens
        join words using (word_id)
        join POStags using(pos_tag_id)
      where dataset_id="""+str(int(dataset_id))+' and text_id='+str(int(text_id))+' order by token_index;'
    tokens = list(map(dict, sql_session.execute(query).fetchall()))
    assert all([tokens[i]['token_index'] == i for i in range(len(tokens))]), 'Token indices not token list indices, text_id: '+str(text_id)
    return tokens


def get_parse(dataset_id, text_id):
    query = """select *
      from constituencyParses
        join parseTags using(parse_tag_id)
      where dataset_id="""+str(int(dataset_id))+' and text_id='+str(int(text_id))+' order by node_index;'
    parse = list(map(dict, sql_session.execute(query).fetchall()))
    for node in parse:
        node['children'] = []
        if node['parent_node_index'] is not None:
            node['parent'] = parse[node['parent_node_index']]
            node['parent']['children'].append(node)
        else:
            node['parent'] = None
    return parse


def get_dependencies(dataset_id, text_id):
    query = """select *
      from dependencies
        join dependencyRelations using(relation_id)
      where dataset_id="""+str(int(dataset_id))+' and text_id='+str(int(text_id))+' order by dependency_id;'
    deps = list(map(dict, sql_session.execute(query).fetchall()))
    return deps


def get_named_entities(dataset_id, text_id):
    query = """select *
      from stanfordNamedEntities
        join stanfordNamedEntityTags using(ner_tag_id)
      where dataset_id="""+str(int(dataset_id))+' and text_id='+str(int(text_id))+' order by ner_index;'
    ners = list(map(dict, sql_session.execute(query).fetchall()))
    return ners


def get_corefs(dataset_id, text_id):
    query = """select * from stanfordCoref
      where dataset_id="""+str(int(dataset_id))+' and text_id='+str(int(text_id))
    corefs = defaultdict(list)
    for mention in map(dict, sql_session.execute(query).fetchall()):
        mention['is_representative'] = (mention['is_representative'] == 1)
        corefs[mention['coref_chain_id']].append(mention)
    return corefs


def get_sentences(dataset_id, text_id):
    query = """select * from sentences
      where dataset_id="""+str(int(dataset_id))+' and text_id='+str(int(text_id))+' order by sentence_index'
    sentences = list(map(dict, sql_session.execute(query).fetchall()))
    return sentences


class Markup(SQL_Base):
    __table__ = sqlalchemy.Table('basic_markup', sql_metadata, autoload=True)

    def __init__(self, dataset_id: int, start: int, end: int, type_name: str, attributes: dict):
        self.dataset_id = dataset_id
        self.text_id = None
        self.start = start
        self.end = end
        self.type_name = type_name
        self.attribute_str = None

        self.attributes = attributes #dataset dependent, but typically a dict. of html attributes Stored in SQL using json

    @reconstructor
    def init(self):
        self.attributes = json.loads(self.attribute_str) if self.attribute_str else {}

    def attributes_to_attribute_str(self):
        """Should only be called when creating these"""
        self.attribute_str = json.dumps(self.attributes) if self.attributes else None
        return self.attribute_str


def load_text_obj(dataset_id: int, text_id: int, load_extras=True) -> Text:
    """Typically you'll want to load the post which will include the text_obj,
      occassionally you'll just want the text obj. This is for the latter"""
    text = sql_session.query(Text).filter_by(dataset_id=dataset_id, text_id=text_id).scalar()
    if load_extras:
        text.load_parse_data()
    return text
