import datetime

from ._sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata
import sqlalchemy
from sqlalchemy.orm import relationship


class Tweet(SQL_Base):
    __table__ = sqlalchemy.Table('tweets', sql_metadata, autoload=True)
    text_obj = relationship("Text", uselist=False)

    def __init__(self, dataset_id: int, tweet_id: int, author_id: int, timestamp: datetime.datetime,
                 in_reply_to_author_id: int, native_tweet_id: str, text_id: int, retweets: int, favorites: int,
                 in_reply_to_tweet_id: int, in_reply_to_native_tweet_id: str, topic_id: int):
        self.dataset_id = dataset_id
        self.tweet_id = tweet_id
        self.author_id = author_id
        self.timestamp = timestamp
        self.in_reply_to_author_id = in_reply_to_author_id
        self.native_tweet_id = native_tweet_id
        self.text_id = text_id
        self.retweets = retweets
        self.favorites = favorites
        self.in_reply_to_tweet_id = in_reply_to_tweet_id
        self.in_reply_to_native_tweet_id = in_reply_to_native_tweet_id
        self.topic_id = topic_id

