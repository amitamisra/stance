'''
    From Rob Abbot's IAC.
    https://bitbucket.org/nlds_iac/internet-argument-corpus

    Provides some paths, the resources list
'''

import os


def _get_root_dir():
    path = os.path.abspath(os.path.dirname(__file__))
    return path

# the extra "''+" ensures it is treated like a str instead of bytes.
# Doesn't matter at all but leads to fewer IDE warnings
root_dir = ''+_get_root_dir() + '/'

