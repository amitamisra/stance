from collections import namedtuple
from ._sql_connect import sql_session, SQL_Base, sql_metadata

import sqlalchemy

Topic_Full_ID = namedtuple("Topic_Full_ID", \
        ["topic_id"])

class Topic(SQL_Base):
    __table__ = sqlalchemy.Table("topics", sql_metadata, autoload=True)

    def __init__(self, topic_id: int, topic: str):
        self.topic_id = topic_id
        self.topic = topic

