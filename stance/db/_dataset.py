from ._discussion import Discussion
from ._sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata
import sqlalchemy
from sqlalchemy import Column, Integer, String, and_


class Dataset(SQL_Base):
    __table__ = sqlalchemy.Table('datasets', sql_metadata, autoload=True)

    def __init__(self, dataset_id, name, source_url, description):
        """Note that the sqlalchemy ORM does not call this,
        so don't put stuff here expecting it to run,
        put it in init() below!"""
        self.dataset_id = dataset_id
        self.name = name
        self.source_url = source_url
        self.description = description

    def __iter__(self):
        return iter(self.get_discussions())

    def get_discussions(self, max_discussions=None, discussion_list=None, topics=None, report_progress=True, lock=None):
        if discussion_list is None:
            discussion_list = self.get_discussion_ids(topics)

        if max_discussions:
            discussion_list = list(discussion_list)  # in case it is a set or something
            discussion_list = discussion_list[:max_discussions]

        for discussion_id in discussion_list:
            assert discussion_id is not None
            discussion = self.load_discussion(discussion_id)
            yield discussion

    def load_discussion(self, discussion_id: int) -> Discussion:
        discussion = sql_session.query(Discussion).filter(
            and_(Discussion.dataset_id == self.dataset_id, Discussion.discussion_id == discussion_id)).scalar()
        return discussion

    def get_discussion_ids(self, topics=None) -> [1, 2]:
        if topics:
            query_str = 'select discussion_id from discussions join topics using(topic_id) where dataset_id=' + str(int(self.dataset_id)) + " and topic in('"  + "', '".join(topics) + "')"
            #TODO: escape topics! & verify iterable
        else:
            query_str = 'select discussion_id from discussions where dataset_id=' + str(int(self.dataset_id)) #catse to int for slightly better security
        query = sql_session.execute(query_str)
        result = query.fetchall()
        discussion_ids = [int(discussion_id[0]) for discussion_id in result]
        return discussion_ids

    def get_author_ids(self, refresh=True) -> {(0, 'authorName')}:
        """Note that by default this updates the mapping and then returns it.
          You can reverse the hash using: {name: sql_id for sql_id, name in dataset.get_author_ids(refresh=False).items()}
        """
        if refresh:
            authors_table = sqlalchemy.Table('authors', sql_metadata,
                Column('dataset_id', Integer),
                Column('author_id', Integer),
                Column('author', String))
            select = sqlalchemy.select(
                columns=[authors_table.c.author_id, authors_table.c.author],
                whereclause=(authors_table.c.dataset_id == self.dataset_id))
            result = select.execute()
            self._author_ids = dict(list(result))
        return self._author_ids

    def load_extras(self):
        #Maybe inheritance would be better?
        try:  # TODO: load from dataset.name
            from .specific_dataset_extras import fourforums as dataset_extras
        except:
            print('Didn\'t load dataset extras')
            return
        dataset_extras._load_dataset_extras(self)


def load_dataset(name: str, load_extras=True) -> Dataset:
    dataset = sql_session.query(Dataset).filter_by(name=name).scalar()
    if load_extras:
        dataset.load_extras()
    return dataset


def get_dataset_id(name: str) -> int:
    dataset = sql_session.query(Dataset).filter_by(name=name).scalar()
    return dataset.dataset_id


def create_dataset(dataset_id: int, name: str, source_url, description: str):
    if sql_session.query(Dataset).filter_by(name=name).count() == 0:
        dataset = Dataset(
            dataset_id=dataset_id,
            name=name,
            source_url=source_url,
            description=description)
        sql_session.add(dataset)
        sql_session.commit()
