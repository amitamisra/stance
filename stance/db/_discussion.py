import operator
from collections import namedtuple

from ._post import Post

import sqlalchemy
from sqlalchemy.orm import relationship
from ._sql_connect import SQL_Base, sql_session, reconstructor, sql_metadata

Discussion_Full_ID = namedtuple('Discussion_Full_ID', ['dataset_id', 'discussion_id'])

topics_table = sqlalchemy.Table('topics', sql_metadata, autoload=True)

class Discussion(SQL_Base):
    #TODO: join topics directly
    __table__ = sqlalchemy.Table('discussions', sql_metadata, autoload=True)
    posts = relationship("Post", backref="discussion")
    initiating_author_obj = relationship('Author')

    def __init__(self, dataset_id: int, discussion_id: int, discussion_url: str, title: str, topic_id: int, initiating_author_id: int, native_discussion_id: int):
        self.dataset_id = dataset_id
        self.discussion_id = discussion_id
        self.discussion_url = discussion_url
        self.title = title
        self.topic_id = topic_id
        self.initiating_author_id = initiating_author_id
        self.native_discussion_id = native_discussion_id
        self.post_dict = None
        self._post_list = None

    @reconstructor
    def init(self):
        """For sqlalchemy ORM"""
        self.initiating_author = self.initiating_author_obj.username if self.initiating_author_obj else None
        # self.authors = set([post.author for post in self.posts])
        self.post_dict = None
        self._post_list = None
        self.topic = sqlalchemy.select([topics_table.c.topic], topics_table.c.topic_id==self.topic_id).execute().scalar()

    def __iter__(self):
        return iter(self.get_posts())

    def full_id(self):
        return Discussion_Full_ID(self.dataset_id, self.discussion_id)

    def get_posts(self) -> [Post]:
        # Other sorting methods might be desirable (e.g. traversal if connected)
        # note that the discussion's posts might form multiple trees, no trees, or contain loops
        if self._post_list is None:
            self._post_list = list(self.posts)
            #TODO: fix!
            self._post_list = [post for post in self._post_list if post.timestamp is not None and post.native_post_id is not None]
            self._post_list.sort(key=operator.attrgetter('timestamp', 'native_post_id'))
            self.post_dict = {post.full_id(): post for post in self._post_list}

            #quote sources
            for post in self._post_list:
                for quote in post.get_all_quotes():
                    source_id = quote.source_full_id()
                    if source_id in self.post_dict:
                        quote.source = self.post_dict[source_id]

            for post in self._post_list:
                if (post.dataset_id, post.discussion_id, post.parent_post_id) in self.post_dict:
                    post._set_parent(self.post_dict[(post.dataset_id, post.discussion_id, post.parent_post_id)])

        return self._post_list


