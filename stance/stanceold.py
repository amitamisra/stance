#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os, configparser
import createFeatures
import file_utilities

def runStance():
    args= readCongigFile()
    inputCsv= args[0]
    outputCsv=args[1]
    targetCol=args[2]
    execute(inputCsv,outputCsv,targetCol)


def readCongigFile():
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','createrFeatures.ini'))
    inputFile = config.get('stance', 'inputFile')
    outputFile= config.get('stance', 'outputFile')
    textCol= config.get('stance', 'textCol')
    arguments=(inputFile,outputFile,textCol)
    return  (arguments)


def execute():
        featureObj= createFeatures.Features()
        rowdicts=file_utilities.readCsv(featureObj.inputCsv, "utf-8", ",")
        featurerowdicts=featureObj.createFeaturesAllRows(rowdicts)
        file_utilities.write_csv_header_unkown(featureObj.outputCsv, featurerowdicts)
        
        
if __name__ == '__main__':
    runStance()