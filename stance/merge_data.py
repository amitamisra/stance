import csv
import sys

if len(sys.argv) != 4:
    print("usage: python3 merge_data.py twitter_train semeval_train topic")
    exit()

train_list = []

for arg in [sys.argv[1],sys.argv[2]]:
    with open(arg) as train_file:
        train_reader = csv.DictReader(train_file)
        for row in train_reader:
            train_list.append(row)

all_train_filename = "all_train_"+sys.argv[3]+".csv"
with open(all_train_filename,'w') as all_train:
    all_train.write('"Stance","ID","Target","Tweet"\n')
    writer = csv.writer(all_train, delimiter=',', quotechar='"',quoting=csv.QUOTE_ALL)
    for row_dict in train_list:
        row_vals = (row_dict['Stance'],row_dict['ID'],row_dict['Target'],row_dict['Tweet'])
        writer.writerow(row_vals)

print("combined file is",all_train_filename)
