#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
Created on Oct 15, 2015
Note: specified encoding as latin-1 during reading as utf-8 was not working
while writing it back as csv it is converted to  utf-8
creates topic specific input files, uses the  topic name as output filename 
@author: amita
'''

import file_utilities
import os, configparser

def runStance(section):
    args= readCongigFile(section)
    input_csv= args[0]
    output_dir=args[1]
    target_list=args[2]
    targetCol=args[3]
    textCol=args[4]
    execute(input_csv,output_dir,target_list,targetCol,textCol)


def readCongigFile(section):
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','separateFilesTargetConfig.ini'))
    inputFile = config.get(section, 'inputFile')
    outputDir= config.get(section, 'outputDir')
    targetList= config.get(section, 'targetList').split(",")
    targetCol= config.get(section, 'targetCol')
    textCol=config.get(section,'textCol')
    arguments=(inputFile,outputDir,targetList,targetCol,textCol)
    return  (arguments)

def removeSemEvalHashTag(rows,targetCol):
    for row in rows:
        text= row[targetCol]
        semEvalhastag=str(text).find("#SemST")
        newtext=text[:semEvalhastag]
        row[targetCol]=newtext
    return rows    
        
        
def execute(input_csv,outputdir,targetList, targetCol,textCol):
    rowstarget=[]
    noOfTargets=len(targetList)
    rows=file_utilities.readCsv(input_csv,"latin-1", '\t')
    colNames=rows[0].keys()
    rows=removeSemEvalHashTag(rows,textCol)
    file_utilities.writeCsv(input_csv[:-4]+".csv",rows,colNames)
    
    for count in range(0,noOfTargets):
        rows_withtarget=[row for row in rows if row[targetCol]==targetList[count]]
        rowstarget.append(rows_withtarget)
        
    for count in range(0,noOfTargets):
        alltargetrows=rowstarget[count]
        filenametarget= os.path.join(outputdir, alltargetrows[0][targetCol]+".csv")
        file_utilities.writeCsv(filenametarget,alltargetrows, alltargetrows[0].keys())
if __name__ == '__main__':
    section='trialData'
    runStance(section)