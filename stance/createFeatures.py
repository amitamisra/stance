#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Oct 17, 2015
Entry Point for Stance classification
Input : A CSV with tweet column
Output: Two CSVs, One with features normalized and another not normalized
@author: amita
'''
import configparser
import csv
import itertools
import sys
from time import time

import arff
import arff_writer
import createNGrams
import file_utilities
import norm_utilities
import os
import read_parses
from CMUTweetTagger import runtagger_parse as CMU_parse
from collections import defaultdict
from liwc import LIWCStance
from liwc import wcc2015
from pmi.MaxPmiFeatureExtractor import MaxPmiFeatureExtractor
from pmi.PmiPoolingFeature import PmiPoolingFeatureExtractor
from sentiment import addSentimentScore


class Features:
    stopword_set = set([".","a","an","the","to","from","'",'"',",","."])
    slang = norm_utilities.build_slang_dict()

    def __init__(self, inputCsv, outputCsv, textCol, stanceCol, mode, stem, stop, pmiPoolPercent, predictedVar,
                 featureList, split, target, ngram_order, dependencyFile, dependency, topic):
        self.featureList=featureList
        self.inputCsv = inputCsv
        self.outputCsv = outputCsv
        self.textCol = textCol
        self.stancCol = stanceCol
        self.predictedVar=predictedVar
        if mode != "train" and mode != "test" and mode != "cv":
            print("Error: Mode must be 'train', 'test', or 'cv'")
            sys.exit(1)
        self.mode = mode
        self.stem = stem
        self.stop= stop
        self.split = split
        self.target = target
        self.order = ngram_order

        if "high_pmi_ngram_count" in self.featureList:
            self.pmiPoolingExtractor = PmiPoolingFeatureExtractor(pmiPoolPercent, order=self.order)
        if "max_pmi" in self.featureList:
            self.maxPmiExtractor = MaxPmiFeatureExtractor(self.order)
        if "high_pmi_in_topic" in self.featureList:
            self.pmi_pool_by_topic = {"legalization of abortion": PmiPoolingFeatureExtractor(pmiPoolPercent, order=self.order, topics=["abortion"]),
                                      "climate change is a real concern": PmiPoolingFeatureExtractor(pmiPoolPercent, order=self.order, topics=["climate change is a real concern"]),
                                      "atheism": PmiPoolingFeatureExtractor(pmiPoolPercent, order=self.order, topics=["atheism"]),
                                      "hillary clinton": PmiPoolingFeatureExtractor(pmiPoolPercent, order=self.order, topics=["hillary clinton"]),
                                      "feminist movement": PmiPoolingFeatureExtractor(pmiPoolPercent, order=self.order, topics=["feminist movement"])}

        self.dependencyFile = dependencyFile
        self.dependency = dependency
        self.topic = topic

        self.liwc_stance=LIWCStance.LIWC_Stance()
        self.afinn=addSentimentScore.AFINN()
        self.binglui=addSentimentScore.BingLui()

    def createngram(self, n, featureVector, rowdict):
        createNGrams.add_text_feature(rowdict, featureVector, n, self.stem,self.stop, Features.stopword_set)
        # modify feature vector and create ngrams
        pass

    def createPOS(self,rowdict,min_n,max_n,featureVector):
        createNGrams.add_pos_feature(rowdict, min_n, max_n, featureVector)
        # modify feature vector
        pass


    def createLIWCBigram(self,rowdict,featureVector):
        # get liwc bigrams : combo of (every cat of token 1, every cat of token 2)
        tokens = dict(sorted([(t['token_index'], t['corrected_token']) for t in rowdict['tokens']]))
        for i in range(len(tokens)-1):
            cats_1 = [cat for cat in self.liwc_stance.liwc_dict.score_word(tokens[i]) if cat not in self.liwc_stance.liwc_ignore]
            cats_2 = [cat for cat in self.liwc_stance.liwc_dict.score_word(tokens[i+1]) if cat not in self.liwc_stance.liwc_ignore]
            cat_bigrams = itertools.product(cats_1,cats_2)
            for cat_bigram in cat_bigrams:
                featureVector["LIWC_bigram:"+cat_bigram[0]+'_'+cat_bigram[1]] += 1

    def createScoreAFINN(self,rowdict,featureVector):
        negativeScore=0
        positiveScore=0
        for token_dict in rowdict['tokens']:
            if float(token_dict["afinnscore"]) < 0 :
                negativeScore=negativeScore + (-1 * float(token_dict["afinnscore"]))
            if float(token_dict["afinnscore"]) > 0 :
                positiveScore=positiveScore + float(token_dict["afinnscore"])
        featureVector["AFINN_POS"]=positiveScore
        featureVector["AFINN_NEG"]=negativeScore

    def createScoreBingLui(self,rowdict,featureVector):
        negativeScore=0
        positiveScore=0
        for token_dict in rowdict['tokens']:
            if float(token_dict["bingluiscore"]) < 0 :
                negativeScore=negativeScore + (-1 * float(token_dict["bingluiscore"]))
            if float(token_dict["bingluiscore"]) > 0 :
                positiveScore=positiveScore + float(token_dict["bingluiscore"])
        featureVector["BINGLUI_POS"]=positiveScore
        featureVector["BINGLUI_NEG"]=negativeScore


    def createLIWCCount(self,rowdict,featureVector):
        """
        Add full text's LIWC category counter to the featureVector
        and add LIWC categories to the token dictionary
        AND add LIWC bigrams to the featureVector
        """
        # get category counts of the entire text using word_category_counter_2015.py

        text = ' '.join([token['corrected_token'] for token in rowdict['tokens']])
        cat_counts = wcc2015.score_text(self.liwc_stance.liwc_dict,text,True)
        for cat in cat_counts:
            if cat not in self.liwc_stance.liwc_ignore:
                featureVector["LIWC_category:"+cat] = cat_counts[cat]


    def createEmoticons(self,rowdict,featureVector):
        """
        count # of emoticons in all tokens
        """
        for token_dict in rowdict['tokens']:
            if token_dict['pos'] == 'E':
                featureVector['CountEmoticons:'] += 1

    def createEandQmarks(self,rowdict,text_field,featureVector):
        """
        count # of ? and ! in all tokens
        """
        featureVector['Count:e_and_q_marks'] += len([char for char in rowdict[text_field] if char in '?!'])

    def createEmphasis(self,rowdict,featureVector):
        """
        count # of bold and allcaps tokens
        """
        for token_dict in rowdict['tokens']:
            if token_dict['allCaps']:
                featureVector['allCaps'] += 1

    def createPooledPmiNgram(self, rowdict, featureVector):
        feature_val = self.pmiPoolingExtractor.featurize(self.ngrams_from_rowdict(rowdict, self.order))
        featureVector["high_pmi_ngram_count"] = feature_val

    def createMaxPmiFeature(self, rowdict, featureVector):
        feature_val = self.maxPmiExtractor.featurize(self.ngrams_from_rowdict(rowdict, self.order), self.topic)
        if feature_val < 0:
            featureVector["max_pmi"] = 0
        else:
            featureVector["max_pmi"] = feature_val

    def createPooledPmiNgramByTopic(self, rowdict, featureVector):
        feature_val = self.pmi_pool_by_topic[self.topic].featurize(self.ngrams_from_rowdict(rowdict, self.order))
        featureVector["high_pmi_in_topic"] = feature_val

    def createDependency(self, rowdict, featureVector):
        createNGrams.add_dependency_feature(rowdict, featureVector, self.dependency)

    def rowdict_to_features(self, rowdict):
        """
        Maps a single rowdict to a feature vector.
        Ensure that this is able to be run in parallel!
        :param rowdict:
        :return:
        """
        featureVector=defaultdict(int)
        Features.normalize_tokens(rowdict, Features.slang)
        self.liwc_stance.add_category_token(rowdict)
        addSentimentScore.sentimentscoreAFINN(self.afinn,rowdict,2,"tokens","corrected_token")
        addSentimentScore.sentimentscoreBingLui(self.binglui,rowdict,2,"tokens","corrected_token")
        addSentimentScore.createCombinedScore(rowdict)

        # remove selected hashtags from text
        if "unigram" in self.featureList and "bigram" in self.featureList and "trigram" in self.featureList:
            n =[1,2,3]
            self.createngram(n,featureVector,rowdict)
        elif "unigram" in self.featureList and "bigram" in self.featureList:
            n =[1,2]
            self.createngram(n,featureVector,rowdict)
        elif "unigram" in self.featureList and "trigram" in self.featureList:
            n =[1,3]
            self.createngram(n,featureVector,rowdict)
        elif "bigram" in self.featureList and "trigram" in self.featureList:
            n =[2,3]
            self.createngram(n,featureVector,rowdict)
        else:
            if "unigram" in self.featureList:
                n=[1]
                self.createngram(n,featureVector,rowdict)
            if "bigram" in self.featureList:
                n=[2]
                self.createngram(n,featureVector,rowdict)
            if "trigram" in self.featureList:
                n=[3]
                self.createngram(n,featureVector,rowdict)

        if "POS_bigram" in self.featureList and "POS_trigram" in self.featureList:
            self.createPOS(rowdict,2,3,featureVector)
        else:
            if "POS_bigram" in self.featureList:
                n=2
                self.createPOS(rowdict,n,n,featureVector)
            if "POS_trigram" in self.featureList:
                n=3
                self.createPOS(rowdict,n,n,featureVector)

        if "LIWCCount" in self.featureList:
            self.createLIWCCount(rowdict,featureVector)
        if "LIWCBigramCount" in self.featureList:
            self.createLIWCBigram(rowdict,featureVector)
        if "AFINNSentiment" in self.featureList :
            self.createScoreAFINN(rowdict,featureVector)
        if "BingLuiSentiment" in self.featureList:
            self.createScoreBingLui(rowdict, featureVector)
        if "emoticons" in self.featureList:
            self.createEmoticons(rowdict, featureVector)
        if "!?" in self.featureList:
            self.createEandQmarks(rowdict, self.textCol, featureVector)
        if "emphasis" in self.featureList:
            self.createEmphasis(rowdict, featureVector)
        if "high_pmi_ngram_count" in self.featureList:
            self.createPooledPmiNgram(rowdict, featureVector)
        if "high_pmi_in_topic" in self.featureList:
            self.createPooledPmiNgramByTopic(rowdict, featureVector)
        if "max_pmi" in self.featureList:
            self.createMaxPmiFeature(rowdict, featureVector)
        if "dependency" in self.featureList:
            self.createDependency(rowdict, featureVector)

        createNGrams.processDict(rowdict, featureVector, self.split,self.stancCol)
        return featureVector

    def createFeaturesAllRows(self,rowdicts):
        """
        Create features for all rowdict inputs. This function starts a multicore map over all the
        rowdicts using the rowdict_to_features method to map each rowdict.
        :param rowdicts:
        :return:
        """
        # Tokenize all the rowdicts first
        Features.add_parses(rowdicts, self.textCol)
        if "dependency" in self.featureList:
            Features.add_dependencies(rowdicts, self.dependencyFile)

        start_time = time()
        featureRows = list(map(self.rowdict_to_features, rowdicts))
        end_time = time()
        mins, secs = times_to_mins_secs(start_time, end_time)
        print("\textracting features, took {0} minutes, {1} seconds".format(mins, secs))


        return featureRows


    def ngrams_from_rowdict(self, rowdict, order):
        """
        Get the ngrams made from a rowdict using corrected tokens
        :param rowdict:
        :param order: ngram order
        :return:
        """
        corrected_tokens = [token["corrected_token"] for token in rowdict["tokens"]]
        return self.ngrams_from_tokens(corrected_tokens, order)



    def ngrams_from_tokens(self, tokens, order):
        """
        :param tokens: A list of tokens as strings
        :param order: ngram order
        :return:
        """
        results = list()
        for i in range(0, len(tokens)):
            n = order if i + order < len(tokens) else len(tokens) - i
            for j in range(1, n + 1):
                results.append(tuple(tokens[k].lower() for k in range(i, i + j)))

        return results



    ############################################################
    # Private helper functions
    ############################################################
    def _filter_stopwords(self, word_list: list) -> list:
        """
        :param word_list: list of strings to filter for stopwords
        :return: a new list with stopwords removed
        """
        return [word for word in word_list if word not in Features.stopword_set]



    @staticmethod
    def add_parses(rowdicts, text_field):
        """
        Add parses from some framework CMU, NLTK, etc.
        Currently implemented for CMU tokenization.
        :param rowdicts: list of dictionaries, each containing row data. This will be modified!
        :return:
        """
        tweets = [rowdict[text_field] for rowdict in rowdicts]
        tokens = CMU_parse(tweets)
        for rowdict, token_list in zip(rowdicts, tokens):
            # third item in triple is the confidence level
            rowdict["tokens"] = [{"token_index": index, "original_token": token_tup[0], "pos": token_tup[1]}
                                 for index, token_tup in zip(range(0, len(token_list)), token_list)]

    @staticmethod
    def add_dependencies(rowdicts, depFile):
        tweet_parses = read_parses.split_file(depFile)
        for rowdict, tweet_parse in zip(rowdicts, tweet_parses):
            rowdict['tweebo_parse'] = read_parses.create_parse_dict(tweet_parse)


    @staticmethod
    def normalize_tokens(rowdict, slang_dict):
        norm_utilities.reformtoken(rowdict)
        for token_dict in rowdict["tokens"]:
            short_tok = norm_utilities.recurr_elim(token_dict["original_token"], token_dict["pos"])

            if token_dict["original_token"] != short_tok:
                token_dict["repeatedOccurence"] = True
            else: token_dict["repeatedOccurence"] = False

            token_dict["corrected_token"] = norm_utilities.normalizeTokens(short_tok, token_dict["pos"], slang_dict)

            if token_dict["corrected_token"].isupper() and token_dict["corrected_token"] != 'I':
                token_dict["allCaps"] = True
            else: token_dict["allCaps"] = False


def get_allowable_features(train_file):
    """
    Gets a set of features stored in a csv or arff file.
    :param train_file: The file to read features from.
    :return:
    """
    if ".csv" in train_file:
        return set(file_utilities.read_csv_header(train_file, "utf-8", ","))
    elif ".arff" in train_file:
        # liac-arff module strips all trailing ' and " in the attribute name, which causes problems
        # because some of our features end in a '.
        # Rolling this custom code for now. It works for our purposes because we only have numeric attributes,
        # but it won't work for non-numerics.
        #
        # The old code that doesn't work because of this bug
        # decoder = arff.ArffDecoder()
        # with open(train_file, "r", encoding="utf-8") as f_in:
        #     data = decoder.decode(f_in, encode_nominal=True, return_type=arff.LOD)
        #     return set((feature_tuple[0] for feature_tuple in data["attributes"]))
        #
        #
        # The new code that mimics most of liac-arff's attribute decoding but fixes the trailing quotes issue.
        # It also decodes the encodings made by arff_writer
        allowable_features = set()
        with open(train_file, "r", encoding="utf-8") as f_in:
            for line in f_in:
                if line.upper().startswith("@ATTRIBUTE"):
                    _, name_and_type = line.split(' ', 1)
                    name_and_type.strip()
                    # Regex taken from liac-arff's _RE_ATTRIBUTE constant
                    match = arff._RE_ATTRIBUTE.match(name_and_type)
                    if match is not None and len(match.groups()) > 1:
                        name = match.group(1)
                        # Strip only the edge quotes from the name if it is quote encoded
                        # This is where liac-arff fails because it strips all quotes from the end and beginning
                        if ((name.startswith("'") and name.endswith("'")) or
                                (name.startswith('"') and name.endswith('"'))):
                            name = name[1:-1]
                    # Reverse the replacements done by the arff_writer.DetailedFeature.sanitize() in reverse order
                    reversed_replacements = arff_writer.DetailedFeature.replacements.copy()
                    reversed_replacements.reverse()
                    for original, new in reversed_replacements:
                        # Reverse the order of replacement from original -> new to new -> original
                        name = name.replace(new, original)
                    allowable_features.add(name)
        return allowable_features
    else:
        raise "unknown format for train_file"


def get_feature_list(feature_dicts):
    """
    Gets the list of all features, sorted alphabetically
    :param feature_dicts: The list of feature dictionaries
    :return: A list of all features
    """
    result = set()
    for feature_dict in feature_dicts:
        result.update(feature_dict.keys())

    result = list(result)
    result.sort()
    return result


def times_to_mins_secs(start_time, end_time):
    """
    :param start_time:
    :param end_time:
    :return: tuple of (mins, secs)
    """
    return (end_time - start_time) // 60, (end_time - start_time) % 60


def write_features_to_csv(feature_dicts, feature_list, output_file):
    """
    Write all the feature dicts to a csv
    :param feature_dicts: The list of feature dictionaries
    :param feature_list: The list of all features
    :param output_file: The output file
    :return:
    """
    def chunks(l, n):
        """
        :param l: List to chunk from
        :param n: Size of chunks
        :return: yields chunks of l of size n
        """
        for i in range(0, len(l), n):
            yield l[i:i+n]

    print("Writing features to a csv")
    start_time = time()
    with open(output_file, 'w', encoding="utf-8") as fh:
        writer = csv.writer(fh)
        # Write the header
        writer.writerow(feature_list)

        # Chunk the processing and writing of the feature_dicts to avoid memory/io errors
        for chunk_of_dicts in chunks(feature_dicts, 1000):
            feature_lists = list()
            for feature_dict in chunk_of_dicts:
                features_as_list = list()
                # Make the dictionary into a representative list matching the header
                for feature in feature_list:
                    if feature in feature_dict:
                        features_as_list.append(feature_dict[feature])
                    else:
                        features_as_list.append(0)

                feature_lists.append(features_as_list)

            writer.writerows(feature_lists)

    end_time = time()
    mins, secs = times_to_mins_secs(start_time, end_time)
    print("\ttook {0} minutes, {1} seconds to write the csv".format(mins, secs))


def runStance():
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','createFeaturesConfig.ini'))
    inputFile = config.get('stance', 'inputFile')
    outputFile= config.get('stance', 'outputFile')
    textCol= config.get('stance', 'textCol')
    stanceCol=config.get('stance', 'stanceCol')
    mode = config.get("stance", "mode")
    train_file = config.get("stance", "trainFile")
    stem = config.get("stance", "stem")
    stop = config.get("stance", "stop")
    pmiPoolPercent = int(config.get("stance", "inTopNPmiPercent"))
    predictedVar=config.get("stance","predictedVar")
    featureList = set(config.get("stance","featureList").split(","))
    split = config.get("stance", "split")
    target = config.get("stance", "target")
    ngram_order = int(config.get("stance", "ngram_order"))
    dependencyFile = config.get("stance", "dependencyFile")
    dependency = config.get("stance", "dependency").split(',')
    topic = config.get("stance", "topic").lower()
    # Make sure topic is valid
    assert(topic == "legalization of abortion" or topic == "climate change is a real concern" or
           topic == "atheism" or topic == "hillary clinton" or topic == "feminist movement")

    execute(inputFile, outputFile, textCol, stanceCol, mode, train_file, stem,stop, pmiPoolPercent, predictedVar,
            featureList, split, target, ngram_order, dependencyFile, dependency, topic)


def execute(inputCsv, outputCsv, textCol, stanceCol, mode, train_file, stem,stop, pmiPoolPercent,
            predictedVar, featureList, split, target, ngram_order, dependencyFile, dependency, topic):
    featureObj = Features(inputCsv, outputCsv, textCol, stanceCol, mode, stem, stop, pmiPoolPercent,
                          predictedVar, featureList, split, target, ngram_order, dependencyFile, dependency,
                          topic)
    rowdicts=file_utilities.readCsv(featureObj.inputCsv, "utf-8", ",")
    featurerowdicts = featureObj.createFeaturesAllRows(rowdicts)
    for featurerowdict in featurerowdicts:
        if featurerowdict["Stance"] == "UNKNOWN":
            featurerowdict["Stance"] = "NONE"

    print("Building feature list from all rowdicts")
    feature_list = get_feature_list(featurerowdicts)
    if featureObj.mode == "test":
        # define allowable features as collected from the training data
        allowable_features = get_allowable_features(train_file)
        finalrowdicts = []
        print("Processing rowdicts to filter out features not in the training set")
        st = time()
        for rowdict in featurerowdicts:
            finaldict = defaultdict(int)
            for feature in rowdict:
                if feature in allowable_features:
                    finaldict[feature] = rowdict[feature]
            finalrowdicts.append(finaldict)
        et = time()
        mins, secs = times_to_mins_secs(st, et)
        print("\ttook {0} mins, {1} secs to process all row dicts".format(mins, secs))
        print("writing arff")
        arff_writer.write(outputCsv, finalrowdicts, classification_feature=stanceCol, features=allowable_features)
    else:
        print("writing arff")
        arff_writer.write(outputCsv, featurerowdicts, classification_feature=stanceCol, features=feature_list)

if __name__ == '__main__':
    runStance()

