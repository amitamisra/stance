#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Nov 14, 2015
add sentiment score for each token, read sentiment score from sentiment dictionary
@author: amita
"""

import operator
from collections import defaultdict

from stance import file_utilities


def check_negation(tokens: list, index: int, n: int) -> bool:
    """
    Check if there is a negation word up to previous n levels. Uses LIWC categories to check for negation words.
    Expects each token dict in tokens to have a "category_list" and "corrected_token" entry.
    @param tokens: A list of token dicts
    @param index: Index of the token in the tokens list where the search starts from
    @param n: The number of levels to search backwards
    @return:
    """
    previous_n_tokens = []
    for token_dict in tokens:
        if index - n <= token_dict["token_index"] < index:
            previous_n_tokens.append((token_dict["corrected_token"], token_dict['category_list']))

    for token, cat_list in previous_n_tokens:
        if "Negations" in cat_list:
            return True

    return False


class AFINN:
    def __init__(self):
        self.sentimentAFINN_abs = file_utilities.load_resource("AFINN-111.csv")
        self.sentimentlist = file_utilities.readCsv(self.sentimentAFINN_abs, "latin-1", ",")
        self.sortedlist = sorted(self.sentimentlist, key=operator.itemgetter("word"))

        # Much faster than checking the list
        self.sentiment_word_lookup = dict()
        for row in self.sentimentlist:
            # This one has no duplicate entries, unlike BingLui
            self.sentiment_word_lookup[row["word"].lower().strip()] = float(row["score"])

    def lookup_token(self, word):
        word = word.lower().strip()
        if word in self.sentiment_word_lookup:
            return self.sentiment_word_lookup[word]
        else:
            return 0


class BingLui:
    def __init__(self):
        self.sentimentBingLui_abs = file_utilities.load_resource("BingLui.csv")
        self.sentimentlist = file_utilities.readCsv(self.sentimentBingLui_abs, "latin-1", ",")
        self.sortedlist = sorted(self.sentimentlist, key=operator.itemgetter("word"))

        # Much faster than checking the list
        self.sentiment_word_lookup = defaultdict(list)
        for row in self.sentimentlist:
            # There are some double entries in the BingLui csv, so we have to add them both
            # but I think they are a mistake, honestly.
            self.sentiment_word_lookup[row["word"].lower().strip()].append(row["sentiment"])

    def lookup_token(self, word):
        word = word.lower().strip()
        if word in self.sentiment_word_lookup:
            entries = self.sentiment_word_lookup[word]
            if entries[0] == 'positive':
                return 1
            else:
                return -1
        else:
            return 0


def add_sentiment_scores(sentiment_lookup, tokens, n, token_key, score_key):
    """
    Gets the sentiment score for each token in a list of tokens using the sentiment_lookup object. Reverses sentiment
    scores if we find a negation n levels backward from the token.
    @param sentiment_lookup: An object implementing lookup_token(word). (Either BingLui or AFINN)
    @param tokens: The list of token dictionaries
    @param n: The number of levels backward to check for a negation
    @param token_key: The dictionary key to use to find the token in the token dictionary, probably either
    "corrected_token" or "original_token"
    @param score_key: Key for adding the sentiment score to the token dictionary.
    e.g. token_dict["binglui"] = sentiment_score
    @return: None.
    """
    for token_dict in tokens:
        token = token_dict[token_key]
        score = sentiment_lookup.lookup_token(token)
        if score != 0:
            token_dict[score_key] = reverse_score_if_negation(score, tokens, token_dict["token_index"], n)
        else:
            token_dict[score_key] = 0


def reverse_score_if_negation(score, tokens, index, n):
    """
    Reverses a sentiment score if it is negated
    @param score: The sentiment score value as a numeric value
    @param tokens: The list of tokens as dictionaries
    @param index: The index in the tokens list of the token that has been scored for sentiment
    @param n: How many levels backward to check for negations
    @return:
    """
    if check_negation(tokens, index, n):
        return score * -1
    else:
        return score

# if __name__ == '__main__':
#     import pprint
#
#     from stance.liwc.liwc_tagger import LIWCTagger
#     from stance.tweet_processing import process_tweets
#
#     pp = pprint.PrettyPrinter()
#     binglui = BingLui()
#     afinn = AFINN()
#     tweet = "I don't love cats."
#     tokens = process_tweets([tweet])[0]["tokens"]
#     liwc_tagger = LIWCTagger()
#     for token in tokens:
#         liwc_tagger.add_categories(token, "corrected_token")
#     add_sentiment_scores(afinn, tokens, 2, "corrected_token", "afinn_score")
#     add_sentiment_scores(binglui, tokens, 2, "corrected_token", "binglui_score")
#     pp.pprint(tokens)
