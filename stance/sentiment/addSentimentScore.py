#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Nov 14, 2015
add sentiment score for each token, read sentiment score from sentiment dictionary
@author: amita
'''
import file_utilities
import operator
from collections import defaultdict
#check if there is a negation word upto previous n levels
def checkNegation(rowdict:dict,token:int,index:int,n:int)-> bool:
    PreviousNtokens=[]
    for token_dict in rowdict["tokens"]:
        if token_dict["token_index"] < index and  token_dict["token_index"] >= index-n :
            PreviousNtokens.append( (token_dict["corrected_token"], token_dict['category_list']) )
    for token,cat_list in PreviousNtokens:
            if "Negations" in cat_list:
                return True
            else:
                continue
    return False

class AFINN:
    def __init__(self):
        self.sentimentAFINN_abs = file_utilities.load_resource("AFINN-111.csv")
        self.sentimentlist= file_utilities.readCsv( self.sentimentAFINN_abs,"latin-1",",")
        self.sortedlist=sorted(self.sentimentlist,key = operator.itemgetter("word"))

        # Much faster than checking the list
        self.sentiment_word_lookup = dict()
        for row in self.sentimentlist:
            # This one has no duplicate entries, unlike BingLui
            self.sentiment_word_lookup[row["word"].lower().strip()] = float(row["score"])

    def getscoreAFINN(self, word):
        word = word.lower().strip()
        if word in self.sentiment_word_lookup:
            return self.sentiment_word_lookup[word]
        else:
            return 0

class BingLui:
    def __init__(self):
        self.sentimentBingLui_abs = file_utilities.load_resource("BingLui.csv")
        self.sentimentlist= file_utilities.readCsv(self.sentimentBingLui_abs,"latin-1",",")
        self.sortedlist=sorted(self.sentimentlist,key = operator.itemgetter("word"))

        # Much faster than checking the list
        self.sentiment_word_lookup = defaultdict(list)
        for row in self.sentimentlist:
            # There are some double entries in the BingLui csv, so we have to add them both
            # but I think they are a mistake, honestly.
            self.sentiment_word_lookup[row["word"].lower().strip()].append(row["sentiment"])

    def getscoreBingLui(self,word):
        word = word.lower().strip()
        if word in self.sentiment_word_lookup:
            entries = self.sentiment_word_lookup[word]
        # row=[row for row in self.sentimentlist if row["word"].lower().strip()==word.lower().strip()]
            if entries[0] == 'positive':
                return 1
            else:
                return -1
        else:
            return 0


def sentimentscoreAFINN(affin,rowdict,n,token_dict,tokenfield):
        for token_dict in rowdict[token_dict]:
            correctedtoken=token_dict[tokenfield]
            score=sentimentscore=affin.getscoreAFINN(correctedtoken)
            tokenindex=token_dict["token_index"]
            newscore=reversescoreifNegation(score,rowdict,correctedtoken,tokenindex,n)
            token_dict["afinnscore"]=newscore

def sentimentscoreBingLui(binglui,rowdict,n,token_dict,tokenfield):
        for token_dict in rowdict[token_dict]:
            correctedtoken=token_dict[tokenfield]
            score=sentimentscore=binglui.getscoreBingLui(correctedtoken)
            tokenindex=token_dict["token_index"]
            newscore=reversescoreifNegation(score,rowdict,correctedtoken,tokenindex,n)
            token_dict["bingluiscore"]=newscore

def createCombinedScore(rowdict):
        for token_dict in rowdict['tokens']:
            if token_dict['bingluiscore'] == 0 and token_dict['afinnscore'] == 0:
                token_dict['combinedscore'] = 0
            elif token_dict['bingluiscore'] < 0 and token_dict['afinnscore'] < 0:
                token_dict['combinedscore'] = -2
            elif token_dict['bingluiscore'] < 0 or token_dict['afinnscore'] < 0:
                token_dict['combinedscore'] = -1
            elif token_dict['bingluiscore'] > 0 and token_dict['afinnscore'] == 0:
                token_dict['combinedscore'] = 1
            elif token_dict['bingluiscore'] == 0 and token_dict['afinnscore'] > 0:
                token_dict['combinedscore'] = 1
            elif token_dict['bingluiscore'] > 0 and token_dict['afinnscore'] > 0:
                token_dict['combinedscore'] = 2

def reversescoreifNegation(score,rowdict,token,index,n):
        checkNeg=checkNegation(rowdict,token,index,n)
        if checkNeg==True:
            score = score*-1
        return score


if __name__ == '__main__':
    afinn=BingLui()
    n=2
    token_dict="tokens"
    tokenfield="corrected_token"
    rowdict={'Target': 'Atheism', 'ID': '101', \
              'tokens': [{'corrected_token': 'I', 'allCaps': False, 'original_token': "I", 'repeatedOccurence': False, 'token_index': 0, 'pos': 'A', 'category_list': ['Negation', 'Friends', 'Word Count', 'Positive Emotion', 'drives', 'affiliation', 'Social Processes']}, \
                         {'corrected_token': "don't", 'allCaps': False, 'original_token': "don't", 'repeatedOccurence': False, 'token_index': 1, 'pos': 'N', 'category_list': ['Negation', 'Friends', 'Word Count', 'Positive Emotion', 'drives', 'affiliation', 'Social Processes']}, \
                         {'corrected_token': 'love', 'allCaps': False, 'original_token': 'love', 'repeatedOccurence': False, 'token_index': 4, 'pos': 'N', 'category_list': ['Negations', 'Friends', 'Word Count', 'Positive Emotion', 'drives', 'affiliation', 'Social Processes']}\
                         ] }
    sentimentscoreBingLui(afinn,rowdict,n,token_dict,tokenfield)
    print (rowdict)
