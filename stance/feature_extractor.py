#!/usr/bin/env python3

import pprint
from time import time
import norm_utilities
from collections import defaultdict
from liwc import wcc2015
from liwc.liwc_tagger import LIWCTagger
from sentiment.sentiment_scoring import AFINN, add_sentiment_scores, BingLui
from tweet_processing import process_tweets


class FeatureExtractor:
    stopword_set = {".", "a", "an", "the", "to", "from", "'", '"', ",", "."}
    slang = norm_utilities.build_slang_dict()

    pp = pprint.PrettyPrinter()

    def __init__(self, stem, stop, feature_list, ngram_order):
        self.feature_list = feature_list
        self.ngram_order = ngram_order
        self.stem = stem
        self.stop = stop
        self.afinn = AFINN()
        self.binglui = BingLui()
        self.liwc_tagger = LIWCTagger()

    def add_liwc_count(self, tokens, feature_vector):
        text = ' '.join([token['corrected_token'] for token in tokens])
        cat_counts = wcc2015.score_text(self.liwc_tagger.liwc_dict, text, True)
        for cat in cat_counts:
            if cat not in self.liwc_tagger.liwc_ignore:
                feature_vector["LIWC_category: {0}".format(cat)] = cat_counts[cat]

    def add_feature(self, feature_vector, name, feature):
        full_name = "{0}_{1}".format(name, feature)
        if full_name not in feature_vector:
            feature_vector[full_name] = 1
        else:
            feature_vector[full_name] += 1

    def stopword_in_tuple(self, tup):
        for word in tup:
            if word in self.stopword_set:
                return True

        return False

    def token_sentiment(self, token):
        return (token["afinn_score"] + token["binglui_score"]) / 2

    def tweet_dict_to_features(self, tweet_dict):
        """
        Maps a single tweet_dict to a feature vector.
        Ensure that this is able to be run in parallel!
        :param tweet_dict:
        :return:
        """
        feature_vector = defaultdict(int)
        tokens = tweet_dict["tokens"]
        parses = tweet_dict["parse"]
        for token in tokens:
            self.liwc_tagger.add_categories(token, "corrected_token")

        token_ngrams = self.ngrams_from_tokens(tokens)
        if "ngram" in self.feature_list:
            ngrams = [tuple(token["stemmed"] for token in token_tuple) for token_tuple in token_ngrams]
            for ngram in ngrams:
                if not self.stopword_in_tuple(ngram):
                    self.add_feature(feature_vector, "{0}-{1}".format("ngram", len(ngram)), ngram)

        if "pos" in self.feature_list:
            for pos_ngram in [tuple(token["pos"] for token in token_tuple) for token_tuple in token_ngrams]:
                if len(pos_ngram) > 1:
                    self.add_feature(feature_vector, "{0}-{1}".format("pos_ngram", len(pos_ngram)), pos_ngram)

        if "liwc_count" in self.feature_list:
            self.add_liwc_count(tokens, feature_vector)

        if "afinn_sentiment" in self.feature_list:
            add_sentiment_scores(self.afinn, tokens, 2, "corrected_token", "afinn_score")
            feature_vector["afinn_score"] = sum([token["afinn_score"] for token in tokens])

        if "binglui_sentiment" in self.feature_list:
            add_sentiment_scores(self.afinn, tokens, 2, "corrected_token", "binglui_score")
            feature_vector["binglui_score"] = sum([token["binglui_score"] for token in tokens])

        if "emoticons" in self.feature_list:
            for token in tokens:
                if token['pos'] == 'E':
                    feature_vector['emoticon_count'] += 1

        if "emphasis" in self.feature_list:
            for token in tokens:
                if token["emphasized"]:
                    feature_vector["emphasized_{0}".format(token["stemmed"])] = 1

        if "dep" in self.feature_list:
            deps = list()
            for dep_index, dep in parses.items():
                if dep["head"] != -1:
                    if dep["head"] == 0:
                        gov_token = "ROOT"
                        gov_token_str = "ROOT"
                    else:
                        gov_index = dep["head"]
                        gov_token = tokens[gov_index - 1]
                        gov_token_str = gov_token["stemmed"]

                    dep_token = tokens[dep_index - 1]
                    deps.append((gov_token, dep_token))
                    feature = "({0} <- {1})".format(gov_token_str, dep_token["stemmed"])
                    self.add_feature(feature_vector, "dep", feature)

        if "opinion_dep" in self.feature_list:
            for dep in deps:
                if dep[0] != "ROOT":
                    gov_sentiment = self.token_sentiment(dep[0])
                    if gov_sentiment > 0:
                        # Has gov_sentiment
                        feature = "(+ <- {0})".format(dep[1]["stemmed"])
                        self.add_feature(feature_vector, "opinion_dep", feature)
                    elif gov_sentiment < 0:
                        feature = "(- <- {0})".format(dep[1]["stemmed"])
                        self.add_feature(feature_vector, "opinion_dep", feature)

                dep_sentiment = self.token_sentiment(dep[1])
                if dep[0] == "ROOT":
                    gov_str = "ROOT"
                else:
                    gov_str = dep[0]["stemmed"]
                if dep_sentiment > 0:
                    # Has gov_sentiment
                    feature = "({0} <- +)".format(gov_str)
                    self.add_feature(feature_vector, "opinion_dep", feature)
                elif dep_sentiment < 0:
                    feature = "({0} <- -)".format(gov_str)
                    self.add_feature(feature_vector, "opinion_dep", feature)

        if "liwc_dep" in self.feature_list:
            for dep in deps:
                if dep[0] != "ROOT":
                    categories = dep[0]["category_list"]
                else:
                    categories = list()
                for category in categories:
                    feature = "({0} <- {1})".format(category, dep[1]["stemmed"])
                    self.add_feature(feature_vector, "LIWC_dep", feature)

                categories = dep[1]["category_list"]
                if dep[0] != "ROOT":
                    gov_str = dep[0]["stemmed"]
                else:
                    gov_str = "ROOT"
                for category in categories:
                    feature = "({0} <- {1})".format(gov_str, category)
                    self.add_feature(feature_vector, "LIWC_dep", feature)

        return feature_vector

    def create_features(self, tweets, remove_hashtags=True):
        tweet_dicts = process_tweets(tweets, remove_hashtags=remove_hashtags)
        feature_vectors = list(map(self.tweet_dict_to_features, tweet_dicts))
        return feature_vectors

    def filter_stopwords(self, word_list: list) -> list:
        """
        :param word_list: list of strings to filter for stopwords
        :return: a new list with stopwords removed
        """
        return [word for word in word_list if word not in self.stopword_set]

    def ngrams_from_tokens(self, tokens):
        """
        :param tokens: A list of tokens as strings
        :param order: ngram order
        :return:
        """
        results = list()
        for i in range(0, len(tokens)):
            n = self.ngram_order if i + self.ngram_order < len(tokens) else len(tokens) - i
            for j in range(1, n + 1):
                results.append(tuple(tokens[k] for k in range(i, i + j)))

        return results


def times_to_mins_secs(start_time, end_time):
    """
    :param start_time:
    :param end_time:
    :return: tuple of (mins, secs)
    """
    return (end_time - start_time) // 60, (end_time - start_time) % 60


if __name__ == "__main__":
    tweets = [
        "This is a test tweet, tweet",
        "Down with @Hillary!",
        "I looooove going to the dentist #Happy :) :D",
        "Abortion is against a woman's #RightToChoose"
    ]
    feature_list = [
        "ngram",
        "pos",
        "liwc_count",
        "afinn_sentiment",
        "binglui_sentiment",
        "emoticons",
        "emphasis",
        # "max_pmi",
        "dep",
        "opinion_dep",
        "liwc_dep",
    ]
    feature_extractor = FeatureExtractor(True, True, feature_list, 3)
    features = feature_extractor.create_features(tweets)
    pp = pprint.PrettyPrinter()
    pp.pprint(features)
