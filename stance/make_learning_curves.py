import csv
import subprocess

import file_utilities
import hashlib
import os
import random
import shutil
import tempfile
from createFeatures import execute

# The folder to create results folders inside of
RESULTS_ROOT = "/home/brian/git/stance/data/learning_curves/no_hashtags"
# Your weka jar location
WEKA_JAR = "/home/brian/software/weka-3-7-13/weka.jar"
# Dep feature types
DEP_TYPES = ["general", "liwc", "sentiment_combined"]
# 100 would be a new feature extraction and ML experiment every 300 instances, 100 from each of favor, against, none
STEP_SIZE = 200
# Stem: boolean
STEM = True
# Stop: boolean
STOP = True

input_dicts = [
    {
        # Input csv to read training instances from
        # "input_file": "/home/brian/git/stance/data/training/feminism/allThreeBalFeminism.csv",
        "input_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/train/allThreeBalFeminism_no_hashtag.csv",
        # SemEval test csv
        # "semeval_test_file": "/home/brian/git/stance/data/final_test_with_labels/feminist_movement.csv",
        "semeval_test_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/test/FeministMovement_no_hashtag.csv",
        # The weka classifier to use. The -t and -T options are automatically generated later
        "weka_command": ["java", "-cp", WEKA_JAR, "weka.classifiers.bayes.NaiveBayesMultinomial", "-o"],
        # Used to name results folder
        "topic": "feminism",
        # What features to use
        # A list of lists of features
        "features": [["unigram"], ["dependency"], ["unigram", "bigram", "dependency", "POS_bigram"]],
        # Topic name for the pmi extractor if used
        "pmi_topic": ""
    },
    {
        # Input csv to read training instances from
        # "input_file": "/home/brian/git/stance/data/training/abortion/allThreeBalAbortion.csv",
        "input_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/train/allThreeBalAbortion_no_hashtag.csv",
        # SemEval test csv
        # "semeval_test_file": "/home/brian/git/stance/data/final_test_with_labels/legalization_of_abortion.csv",
        "semeval_test_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/test/LegalizationofAbortion_no_hashtag.csv",
        # The weka classifier to use. The -t and -T options are automatically generated later
        "weka_command": ["java", "-cp", WEKA_JAR, "weka.classifiers.bayes.NaiveBayesMultinomial", "-o"],
        # Used to name results folder
        "topic": "abortion",
        # What features to use
        "features": [["unigram"], ["dependency"], ["unigram", "bigram", "dependency"]],
        # Topic name for the pmi extractor if used
        "pmi_topic": ""
    },
    {
        # Input csv to read training instances from
        # "input_file": "/home/brian/git/stance/data/training/atheism/allThreeBalAtheism.csv",
        "input_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/train/allThreeBalAtheism_no_hashtag.csv",
        # SemEval test csv
        "semeval_test_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/test/Atheism_no_hashtag.csv",
        # The weka classifier to use. The -t and -T options are automatically generated later
        "weka_command": ["java", "-cp", WEKA_JAR, "weka.classifiers.bayes.NaiveBayesMultinomial", "-o"],
        # Used to name results folder
        "topic": "atheism",
        # What features to use
        "features": [["unigram"], ["dependency"], ["unigram", "bigram", "dependency"]],
        # Topic name for the pmi extractor if used
        "pmi_topic": ""
    },
    {
        # Input csv to read training instances from
        # "input_file": "/home/brian/git/stance/data/training/hillary/allThreeBalHillary.csv",
        "input_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/train/allThreeBalHillary_no_hashtag.csv",
        # SemEval test csv
        # "semeval_test_file": "/home/brian/git/stance/data/final_test_with_labels/hillary_clinton.csv",
        "semeval_test_file": "/home/brian/git/stance/data/learning_curves/no_hashtags/test/HillaryClinton_no_hashtag.csv",
        # The weka classifier to use. The -t and -T options are automatically generated later
        "weka_command": ["java", "-cp", WEKA_JAR, "weka.classifiers.bayes.NaiveBayesMultinomial", "-o"],
        # Used to name results folder
        "topic": "hillary",
        # What features to use
        "features": [["unigram"], ["dependency"]],
        # Topic name for the pmi extractor if used
        "pmi_topic": ""
    },
]


def get_parses(tweets):
    """
    Parse a list of text and return the path to the parse file
    @param tweets: A list of tweets as strings
    @return: The path to the parse file
    """
    # Write tweets to text file, line-by-line
    parse_filename = hashlib.md5("".join(tweets).encode("utf-8")).hexdigest() + ".parses"
    parse_dir = os.path.join(file_utilities.PROJECT_DIR, "parses")
    if not os.path.exists(parse_dir):
        os.mkdir(parse_dir)
    output_file = os.path.join(parse_dir, parse_filename)
    if not os.path.exists(output_file):
        print("Parses do not exist... ")
        tmp_txt_file = tempfile.NamedTemporaryFile("w")
        tweets_by_line = "\n".join(tweets) + "\n"
        tmp_txt_file.writelines(tweets_by_line)
        tmp_txt_file.flush()

        # Do parsing
        current_dir = os.getcwd()
        parser_path = file_utilities.load_resource("TweeboParser")
        os.chdir(parser_path)
        subprocess.call("./run.sh " + tmp_txt_file.name, shell=True)
        os.chdir(current_dir)
        tmp_txt_file.close()
        prediction_file = tmp_txt_file.name + ".predict"
        shutil.move(prediction_file, output_file)

    return output_file


def create_parses_from_parses(favor_parses, against_parses, none_parses, num_from_each):
    """
    Creates a new NamedTemporaryFile with num_from_each included from each of the separate parse files
    :param favor_parses: The absolute path of the file with the full list of favor parses
    :param against_parses: The absolute path of the file with the full list of against parses
    :param none_parses: The absolute path of the file with the full list of none parses
    :param num_from_each: The number of parses to use from each
    :return: The NamedTemporaryFile with the smaller list of parses
    """

    def take_n(f, n):
        with open(f, 'r') as fh:
            lines = fh.read().split("\n")
        individual_tweet_parses = list()
        group = list()
        taken = 0
        for line in lines:
            if taken >= n:
                break
            group.append(line)
            if line.strip() == "":
                individual_tweet_parses += group
                group = list()
                taken += 1

        return "\n".join(individual_tweet_parses) + "\n"

    all_lines = (take_n(favor_parses, num_from_each) + take_n(against_parses, num_from_each) +
                 take_n(none_parses, num_from_each))

    temp_combined_parses = tempfile.NamedTemporaryFile("w")
    temp_combined_parses.writelines(all_lines)
    temp_combined_parses.flush()

    return temp_combined_parses


def get_tweets(rows, header):
    return [row[header.index("Tweet")] for row in rows]


def do_feature_extraction(input_file, semeval_test_file, train_dir, test_dir, results_file, only_fscores, cv_results,
                          features, pmi_topic):
    if not os.path.exists(train_dir):
        os.makedirs(train_dir)

    if not os.path.exists(test_dir):
        os.makedirs(test_dir)

    with open(input_file, 'r') as fh:
        reader = csv.reader(fh)
        header = next(reader)
        rows = [row for row in reader]
        favor_rows = [row for row in rows if row[header.index("Stance")] == "FAVOR"]
        against_rows = [row for row in rows if row[header.index("Stance")] == "AGAINST"]
        none_rows = [row for row in rows if row[header.index("Stance")] == "NONE"]
        random.seed(100)
        random.shuffle(favor_rows)
        random.shuffle(against_rows)
        random.shuffle(none_rows)

    with open(semeval_test_file, 'r') as fh:
        reader = csv.reader(fh)
        semeval_header = next(reader)
        rows = [row for row in reader]
        semeval_test_parses = get_parses(get_tweets(rows, semeval_header))

    favor_parses = get_parses(get_tweets(favor_rows, header))
    against_parses = get_parses(get_tweets(against_rows, header))
    none_parses = get_parses(get_tweets(none_rows, header))

    # We assume our input files are balanced, so this should always be true, and it makes things easier to deal with
    # if it is true.
    assert (len(favor_parses) == len(against_parses) == len(none_parses))
    max_step = len(favor_rows)

    steps = list(range(STEP_SIZE, max_step, STEP_SIZE))
    if max_step % STEP_SIZE != 0:
        steps.append(len(favor_rows))

    # Do all the feature creation and parsing
    for num_instances in steps:
        print("Creating train and test for first {0} instances, {1} of each label".format(num_instances * 3,
                                                                                          num_instances))

        # Define train and test output files
        train_output = os.path.abspath(os.path.join(train_dir, "{0}_features.arff".format(num_instances * 3)))
        test_output = os.path.abspath(os.path.join(test_dir, "{0}_features.arff".format(num_instances * 3)))

        if os.path.exists(train_output) and os.path.exists(test_output):
            continue

        # Write a temporary shorter file
        input_file = tempfile.NamedTemporaryFile("w")
        writer = csv.writer(input_file)
        writer.writerow(header)
        writer.writerows(favor_rows[:num_instances])
        writer.writerows(against_rows[:num_instances])
        writer.writerows(none_rows[:num_instances])
        input_file.flush()

        # Get parses for the equivalent of the temporary shorter file
        # if "dependency" in features:
        if "dependency" in features:
            parse_file = create_parses_from_parses(favor_parses, against_parses, none_parses, num_instances)
            parse_file_name = parse_file.name
        else:
            parse_file_name = ""

        if not os.path.exists(train_output):
            # Create features for train
            execute(input_file.name,
                    train_output,
                    "Tweet",
                    "Stance",
                    "train",
                    "",
                    True,
                    True,
                    0,
                    "",
                    features,
                    "individual",
                    "all",
                    2,
                    parse_file_name,
                    DEP_TYPES,
                    pmi_topic
                    )

        if not os.path.exists(test_output):
            # Create features for test
            execute(semeval_test_file,
                    test_output,
                    "Tweet",
                    "Stance",
                    "test",
                    train_output,
                    True,
                    True,
                    0,
                    "",
                    features,
                    "individual",
                    "all",
                    2,
                    semeval_test_parses,
                    DEP_TYPES,
                    pmi_topic)


def get_fscores(weka_output: str):
    """
    Take a chunch of weka training/cv output and get the fscores on the test/test-fold set
    :param weka_output:
    :return:
    """
    # Chunk the result to get the f-scores in the second (test) half
    relevant_res = weka_output.split("Error on test data")
    if len(relevant_res) == 1:
        # It's a cv result
        relevant_res = weka_output.split("Stratified cross-validation")
    relevant_res = relevant_res[1]

    scores = list()
    active = 0
    for line in relevant_res.split("\n"):
        if active > 0:
            # Take the f-measure
            scores.append(float(line.strip().split()[4]))
            active -= 1
        if "TP Rate" in line:
            # Let's take the next three lines
            active = 3

    # Write the f-scores
    scores.append((scores[0] + scores[1]) / 2)
    return scores


def do_ml(train_dir, test_dir, results_file, only_fscores, cv_results, weka_command):
    with open(results_file, 'w') as fh, open(only_fscores, 'w') as scores_fh, open(cv_results, 'w') as cv_fh:
        train_test_writer = csv.writer(scores_fh)
        cv_writer = csv.writer(cv_fh)
        train_test_writer.writerow(["num_instances", "f_against", "f_favor", "f_none", "f_average"])
        cv_writer.writerow(["num_instances", "f_against", "f_favor", "f_none", "f_average"])
        for f in os.listdir(train_dir):
            train = os.path.abspath(os.path.join(train_dir, f))
            test = os.path.abspath(os.path.join(test_dir, f))
            train_test_command = weka_command + ["-t", train, "-T", test]
            train_test_res = subprocess.check_output(train_test_command).decode("utf-8")
            fh.write("RESULTS FOR TRAIN FILE: {0}".format(train))
            fh.write(train_test_res)

            # Get the f-scores for train_test
            num_instances = int(f.split("_")[0])
            train_test_fscores = get_fscores(train_test_res)
            train_test_writer.writerow([num_instances] + train_test_fscores)

            # Now do cv and get f-scores
            cv_command = weka_command + ["-t", train]
            cv_res = subprocess.check_output(cv_command).decode("utf-8")
            cv_fscores = get_fscores(cv_res)
            cv_writer.writerow([num_instances] + cv_fscores)


if __name__ == "__main__":
    for input_dict in input_dicts:
        for feature_set in input_dict["features"]:
            print("==========")
            print("Extracting learning curve for {0} with features: {1}".format(input_dict["topic"], feature_set))
            print("==========")
            features_joined = "_".join(feature_set)
            topic_root = os.path.join(RESULTS_ROOT, input_dict["topic"])
            if not os.path.exists(topic_root):
                os.makedirs(topic_root)
            step_root = os.path.join(topic_root, "{0}_step/".format(STEP_SIZE))
            if not os.path.exists(step_root):
                os.makedirs(step_root)

            # Where to write the full results of ML experiments
            results_file = os.path.join(step_root, "{0}_results.txt".format(features_joined))
            # Where to write a csv with only with rows of [num_instances, f_against, f_favor, f_none, f_average] where
            # f_average is the average of against and favor
            only_fscores = os.path.join(step_root, "{0}_fscores.csv".format(features_joined))
            # Where to write a csv of the CV results (f scores only)
            cv_results = os.path.join(step_root, "{0}_cv_scores.csv".format(features_joined))
            # Where to save training arff files
            train_dir = os.path.join(topic_root, "features", "train", features_joined)
            # Where to save testing arff files
            test_dir = os.path.join(topic_root, "features", "test", features_joined)

            do_feature_extraction(input_dict["input_file"], input_dict["semeval_test_file"], train_dir, test_dir,
                                  results_file, only_fscores, cv_results, feature_set, input_dict["pmi_topic"])
            do_ml(train_dir, test_dir, results_file, only_fscores, cv_results, input_dict["weka_command"])
