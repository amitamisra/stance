library("ggplot2")

df.dep.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/feminism/200_step/dependency_fscores.csv")
df.uni.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/feminism/200_step/unigram_fscores.csv")
df.best.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/feminism/200_step/unigram_bigram_dependency_POS_bigram_fscores.csv")
plot <- ggplot(data.frame(df.dep.test$num_instances, df.dep.test$f_average, df.uni.test$f_average, df.best.test$f_average)) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, shape="Dependency", color="Dependency")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, color="Dependency"), se=FALSE) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, shape="Unigram", color="Unigram")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, color="Unigram"), se=FALSE) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.best.test$f_average, shape="Uni+Bi+Dependency+POS_bi", color="Uni+Bi+Dependency+POS_bi")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.best.test$f_average, color="Uni+Bi+Dependency+POS_bi"), se=FALSE) +
  xlab("Number of Instances") +
  ylab("Average F-score") +
  theme(legend.position = c(0.3, 0.85)) + 
  theme(legend.title=element_blank()) +
  ylim(0.4, 0.7) +
  ggtitle("Feminism on SemEval Final Test")
plot
ggsave("/home/brian/git/stance/data/learning_curves/no_hashtags/feminism/200_step/fem-200-step-learning.png", plot)

df.dep.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/abortion/200_step/dependency_fscores.csv")
df.uni.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/abortion/200_step/unigram_fscores.csv")
df.best.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/abortion/200_step/unigram_bigram_dependency_fscores.csv")
plot <- ggplot(data.frame(df.dep.test$num_instances, df.dep.test$f_average, df.uni.test$f_average, df.best.test$f_average)) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, shape="Dependency", color="Dependency")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, color="Dependency"), se=FALSE) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, shape="Unigram", color="Unigram")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, color="Unigram"), se=FALSE) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.best.test$f_average, shape="Uni+Bi+Dependency", color="Uni+Bi+Dependency")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.best.test$f_average, color="Uni+Bi+Dependency"), se=FALSE) +
  xlab("Number of Instances") +
  ylab("Average F-score") +
  theme(legend.position = c(0.25, 0.85)) + 
  theme(legend.title=element_blank()) +
  ylim(0.4, 0.7) +
  ggtitle("Abortion on SemEval Final Test")
plot
ggsave("/home/brian/git/stance/data/learning_curves/no_hashtags/abortion/200_step/abor-200-step-learning.png", plot)

df.dep.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/atheism/200_step/dependency_fscores.csv")
df.uni.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/atheism/200_step/unigram_fscores.csv")
df.best.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/atheism/200_step/unigram_bigram_dependency_fscores.csv")
plot <- ggplot(data.frame(df.dep.test$num_instances, df.dep.test$f_average, df.uni.test$f_average, df.best.test$f_average)) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, shape="Dependency", color="Dependency")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, color="Dependency"), se=FALSE) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, shape="Unigram", color="Unigram")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, color="Unigram"), se=FALSE) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.best.test$f_average, shape="Uni+Bi+Dependency", color="Uni+Bi+Dependency")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.best.test$f_average, color="Uni+Bi+Dependency"), se=FALSE) +
  xlab("Number of Instances") +
  ylab("Average F-score") +
  theme(legend.position = c(0.7, 0.2)) + 
  theme(legend.title=element_blank()) +
  ylim(0.4, 0.7) +
  ggtitle("Atheism on SemEval Final Test")
plot
ggsave("/home/brian/git/stance/data/learning_curves/no_hashtags/atheism/200_step/ath-200-step-learning.png", plot)

df.dep.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/hillary/200_step/dependency_fscores.csv")
df.uni.test <- read.csv("/home/brian/git/stance/data/learning_curves/no_hashtags/hillary/200_step/unigram_fscores.csv")
plot <- ggplot(data.frame(df.dep.test$num_instances, df.dep.test$f_average, df.uni.test$f_average)) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, shape="Dependency", color="Dependency")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.dep.test.f_average, color="Dependency"), se=FALSE) +
  geom_point(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, shape="Unigram", color="Unigram")) +
  geom_smooth(aes(x=df.dep.test$num_instances, y=df.uni.test$f_average, color="Unigram"), se=FALSE) +
  xlab("Number of Instances") +
  ylab("Average F-score") +
  theme(legend.position = c(0.25, 0.85)) + 
  theme(legend.title=element_blank()) +
  ylim(0.4, 0.7) +
  ggtitle("Hillary on SemEval Final Test")
plot
ggsave("/home/brian/git/stance/data/learning_curves/no_hashtags/hillary/200_step/hil-200-step-learning.png", plot)


