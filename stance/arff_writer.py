import random
import os
from operator import attrgetter
from collections import defaultdict
import json
import sys
import random


class random_guard:
    def __init__(self, random_seed=None):
        self.random_seed = random_seed
    def __enter__(self):
        if self.random_seed is not None:
            self.original_random_state = random.getstate()
            random.seed(self.random_seed)
        return
    def __exit__(self, type, value, traceback):
        if self.random_seed is not None:
            random.setstate(self.original_random_state)


def write(filename, instances, relation=None, comments=None, sparse=True, instance_keys=None, features=None, sort_features=True, classification_feature=None, enumerate_classification_feature=True, use_compression=False, store_ids=False, write_many=False, minimum_instance_counts_for_features=0, derive_features=True, random_seed=None):
    """Writes arff file(s)
    @param filename: The arff file to write, or, if write_many=True, the foldername to write arffs into
    @param instances: A list or dict of instance feature vectors (dicts)
    @param sparse: Whether the arff file(s) should be sparse or not
    @param features: A list of features to use. Use original names or DetailedFeature()s as returned by write().
    
    """
    assert( len(instances)>0)
    
    instances, instance_keys = get_instances_and_keys(instances, instance_keys, random_seed)
    detailed_features = get_features(instances, features, derive_features, sort_features, classification_feature, enumerate_classification_feature, minimum_instance_counts_for_features)
    
    for output_file, feature_list in get_output_files(filename, detailed_features, write_many):
        if store_ids and instance_keys != None:
            write_comment_line('Instance_ids: '+json.dumps(instance_keys), output_file) #safer than exec() and easier than parsing
        write_comments(comments, output_file)
        write_header(relation, feature_list, output_file)
        
        #For efficiency reasons, sometimes it is better to iterate over the 
        #  instance and not the feature list, the dict allows quick lookup
        feature_dict = dict()
        feature_number = 0
        for detailed_feature in feature_list:
            feature_dict[detailed_feature.original_name] = (detailed_feature, feature_number)
            feature_number+=1
            
        for instance in instances:
            write_instance(instance, feature_list, feature_dict, sparse, output_file)
    return detailed_features, instance_keys

def write_header(relation, feature_list, output_file):
    output_file.write('@RELATION stance_data\n')
    for detailed_feature in feature_list:
        if detailed_feature.feature_type == 'enumeration':
            values = "{"+(','.join(sorted(detailed_feature.values)))+'}'
            output_file.write('@ATTRIBUTE '+detailed_feature.sanitized_name+' '+values+'\n')
        else:
            output_file.write('@ATTRIBUTE '+detailed_feature.sanitized_name+' '+detailed_feature.feature_type+'\n')
    output_file.write('@DATA'+'\n')


def write_instance(instance, feature_list, feature_dict, sparse, output_file):
    instance_feature_list = list()

    if sparse: output_file.write('{')
    
    if sparse and len(instance)**2 < len(feature_list):
        for raw_name, raw_value in list(instance.items()):
            obj = feature_dict.get(raw_name)
            if obj == None: continue
            detailed_feature, feature_number = obj
            value = detailed_feature.sanitize(raw_value)
            instance_feature_list.append((feature_number, value))
        instance_feature_list = sorted(instance_feature_list, key=lambda x:x[0])
        instance_feature_list = [ str(feature_number)+' '+value for feature_number, value in instance_feature_list]
    else:
        feature_number=0
        for detailed_feature in feature_list:
            raw_value = instance.get(detailed_feature.original_name)
            if raw_value == None:
                if sparse:
                    feature_number+=1
                    continue
                raw_value = 0
            value = detailed_feature.sanitize(raw_value)
            feature = str(feature_number)+' '+value if sparse else value
            instance_feature_list.append(feature)
            feature_number+=1
    output_file.write(','.join(instance_feature_list))
    if sparse: output_file.write('}')
    output_file.write('\n')

def get_instances_and_keys(instances, instance_keys, random_seed=None):
    if hasattr(instances, 'items'):
        if instance_keys == None:
            instance_keys = list(instances.keys())
            with random_guard(random_seed):
                random.shuffle(instance_keys)
        instances = [instances[key] for key in instance_keys]
    return instances, instance_keys

def get_output_files(filename, detailed_features, write_many):
    if not write_many:
        dirname = os.path.dirname(filename).strip()
        if dirname.strip() is not '' and not os.path.exists(dirname):
            os.makedirs(dirname)
        yield open(filename, 'w'), detailed_features
    else:
        assert not filename.endswith('.arff'), 'Writing many arffs, but passed a foldername ending in ".arff": <'+filename+'>'
        if not os.path.exists(filename):
            os.makedirs(filename)

        #write an all.arff file
        yield open(filename+'/all.arff', 'w'), detailed_features

        features_by_prefix = defaultdict(list)
        for detailed_feature in detailed_features:
            features_by_prefix[detailed_feature.prefix].append(detailed_feature)

        if len(list(features_by_prefix.keys())) > 10 and len(list(features_by_prefix.keys()))>0.25*len(detailed_features):
            print('Warning: Attempting to write MANY arff files, are prefixes specified correctly? (e.g. "unigram:dog")')

        for prefix, feature_list in list(features_by_prefix.items()):
            yield open(filename+'/'+prefix+'.arff', 'w'), feature_list

class DetailedFeature():
    """@param original_name: The feature's original name
    @param feature_type: 'numeric', 'string', 'date', 'enumeration' (aka nominal)
    @param enumerations: A list of possible values for enumeration type features
    
    """
    def __init__(self, original_name, feature_type = 'numeric'):
        if isinstance(original_name, DetailedFeature):
            self.original_name = original_name.original_name
            self.sanitized_name = original_name.sanitized_name
            self.feature_type = original_name.feature_type
            self.values = original_name.values
            self.prefix = original_name.prefix            
        else:
            self.original_name = original_name
            self.sanitized_name = self.sanitize(original_name, True)
            self.feature_type = feature_type
            self.values = set()
            self.prefix = original_name.split(':')[0]
        self.count = 0
        
    replacements = [('\\','\\\\'),('"','\\"'),('\n','\\n'),('\r','\\n')]
    def sanitize(self, value, is_feature_name=False):
        """@note: this is not the spot to fix unicode errors since multiple 
            attributes could easily end up with the same name and that would be bad. 
            That could still happen with these replacements, but isn't likely
        """
        if value==True:  value=1
        if value==False: value=0
        
        value = str(value)
        if is_feature_name and not value[0].isalpha(): value = 'a__'+value #documentation says attribute names must start with alphabetic character
        for original, new in DetailedFeature.replacements:
            value = value.replace(original,new)
        if not value.isalnum(): value = '"'+value+'"' #Todo - allow underscores too
        return value
    
    def update_values(self, value):
        self.count+=1
        value_type = self.get_type(value)
        if value_type != self.feature_type:
            if value_type == 'enumeration':
                self.feature_type = 'enumeration'
        self.values.add(self.sanitize(value))
        
    def get_type(self, value):    #todo, date
        try:
            float(value) #captures booleans too
            return 'numeric'
        except:
            pass
        return 'enumeration'
        
def get_features(instances, features, derive_features, sort_features, classification_feature, enumerate_classification_feature, minimum_instance_counts_for_features):
    detailed_features = dict()

    if hasattr(instances, 'items'): instances = list(instances.values())
    if features==None: features = list()
    
    for feature in features:
        if isinstance(feature, DetailedFeature):
            detailed_features[feature.original_name]=DetailedFeature(feature)
        else:
            detailed_features[feature]=DetailedFeature(feature)
    
    if derive_features:
        discover_features(detailed_features, instances)
    
    if derive_features and minimum_instance_counts_for_features > 0:
        for detailed_feature in list(detailed_features.values()):
            if detailed_feature.count < minimum_instance_counts_for_features:
                del detailed_features[detailed_feature.original_name]
                
    if classification_feature != None:
        if enumerate_classification_feature:
            detailed_features[classification_feature].feature_type = 'enumeration'
        detailed_classification_feature = detailed_features[classification_feature]
        del detailed_features[classification_feature]
    
    if sort_features:
        full_features_list = sorted(list(detailed_features.values()), key=attrgetter('sanitized_name'))
    else:
        full_features_list = list(detailed_features.values())
    if detailed_classification_feature != None:
        full_features_list.append(detailed_classification_feature)

    return full_features_list

def discover_features(detailed_features, instances):
    for instance in instances:
        for feature, value in list(instance.items()):
            if feature not in detailed_features:
                detailed_features[feature]=DetailedFeature(feature)
            detailed_features[feature].update_values(value)

def write_comments(comments, output_file):
    if comments == None: return
    if not isinstance(comments,list):
        comments = comments.split('\n')
    for line in comments:
        write_comment_line(line, output_file)

def write_comment_line(line, output_file):
    if not line.startswith('%'):
        output_file.write('%')
    output_file.write(line)
    if not line.endswith('\n'):
        output_file.write('\n')
