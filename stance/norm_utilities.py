import re
import sys
from pathlib import Path

import enchant
from unidecode import unidecode


#TODO: Take list of dictionaries and normalize
#add modified token feature - Done
#remove url - Done
#change username tokens to be shown as user reference - Done
#change to lexicon - Done
#return modified dictionary list

def checkRepeat(token, pos):
    prev_char = ''
    char_count, curr_index, start_index = 0, 0, 0
    elong_list = []

    ignore = ['@', '~', ',', 'U', 'E']

    if pos in ignore or re.match(r'[0-9,]+', token) or token[0] == '@':
        return []

    for char in token:
        if char == prev_char:
            if char_count < 3:
                if char_count == 0:
                    start_index = curr_index - 1
                    char_count += 2
                else:
                    char_count += 1
        else:
            if char_count > 2:
                elong_list.append((start_index + 2, curr_index))

            char_count = 0
            prev_char = char

        curr_index += 1

    if char_count > 2:
        elong_list.append((start_index + 2, -1))

    return elong_list

def reduce(token, elong_list):
    end_point = 0
    partition = False

    new_token = ''
    if elong_list is not None:
        for start, stop in elong_list:
            if not partition:
                new_token += token[0:start]
                partition = True
            else:
                new_token += token[end_point:start]

            end_point = stop

        if end_point <= len(token) - 1 and end_point != -1:
            new_token += token[end_point:]

    return new_token

def recurr_elim(token, pos):
    mod_list = checkRepeat(token,pos)
    if len(mod_list) > 0:
        new_token = reduce(token, mod_list)
        return new_token
    else:
        return token


def normalizeTokens(token, pos, slang_dict):
    ignore = ['#', '~', ',', 'E', '^']
    token = token.replace("\\\'", "\'")
    if token[:2] == 'b\'' or token[:2] == 'b\"':
        token = replaceToken(token[2:], slang_dict)
    elif token == 'RT':
        token = ' '
    elif pos == '@' or token[0] == '@':
        token = 'user_ref'
    elif pos == 'U':
        token = "url"
    else:
        if pos not in ignore:
            token = replaceToken(token, slang_dict)

    return token

speller = enchant.Dict('en_us')
def replaceToken(token, slang_dict):
    if not speller.check(token):
        if token in slang_dict:
            if len(slang_dict[token].split(' ')) == 1:
                return slang_dict[token]
        elif token[0] == '#':
            return token

    return token

def default_normalization_dict():
    parentpapth= Path(__file__).parent.parent
    return str(parentpapth.joinpath("resources", "emnlp2012-lexnorm", "emnlp_dict.txt"))

def build_slang_dict():
    filename = default_normalization_dict()
    slang_dict = {}
    slang_fh = open(filename, 'r')
    for line in slang_fh:
        slang_def = line.rstrip().split('\t')
        key = slang_def[0]
        slang_dict[slang_def[0]] = slang_def[1]

    return slang_dict


def replacetokenForWeka(token):
    if token["original_token"]=="'" :
        token["original_token"]='"'

#convert to ascii representation
def reformtoken(rowdict):
    for token_dict in rowdict["tokens"]:
        token_dict["original_token"]=ascii_only_unicodeerror(token_dict["original_token"])

def ascii_only_unicodeerror(text):
    try:
        text=unidecode(text)
        return text
    except Exception as e:
        print (str(e))
        sys.exit(-1)

