#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Nov 19, 2015
called from preprocess to make balanced files for twitter data.
call from main to random tweest for each topic based on number of instances in balancefile for each topic and 
 topiccount for each topic  in PreProcessconfigFiles
@author: amita
'''
from stance import file_utilities
import os
import configparser
import pandas 
import norm_utilities
import random
import re
import random


othercountclimate=100

def calculaterandomtopic(section):
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    otherTopiccountAbortion=int(config.get(section,"OtherTopiccountAbortion"))
    otherTopiccountAtheism=int(config.get(section,"OtherTopiccountAtheism"))
    otherTopiccountClimate=int(config.get(section,"OtherTopiccountClimate"))
    otherTopiccountFeminism=int(config.get(section,"OtherTopiccountFeminism"))
    otherTopiccountHillary=int(config.get(section,"OtherTopiccountHillary"))
    abortionbalFile=config.get(section, "abortionbal")
    atheismbalFile=config.get(section,  "atheismbal")
    climatebalFile=config.get(section, "climatebal")
    feminismbalFile=config.get(section, "feminismbal")
    hillarybalFile=config.get(section, "hillarybal")
    
    abortionbal=file_utilities.readCsv(abortionbalFile, "utf-8", ",")
    atheismbal=file_utilities.readCsv(atheismbalFile, "utf-8", ",")
    climatebal=file_utilities.readCsv(climatebalFile, "utf-8", ",")
    feminismbal=file_utilities.readCsv(feminismbalFile, "utf-8", ",")
    hillarybal=file_utilities.readCsv(hillarybalFile, "utf-8", ",")
    randomCountAbortion= len(abortionbal) - otherTopiccountAbortion*3 - othercountclimate
    randomCountAtheism=len(atheismbal) - otherTopiccountAtheism*3 - othercountclimate
    randomCountClimate=len(climatebal) - otherTopiccountClimate*4 
    randomCountFeminism=len(feminismbal) - otherTopiccountFeminism*3 - othercountclimate
    randomCountHillary=len(hillarybal) - otherTopiccountHillary*3 - othercountclimate
    print("\nrandomCountAbortion: "+ str(randomCountAbortion))
    print("\nrandomCountAtheism: " + str(randomCountAtheism))
    print("\nrandomCountClimate: " + str(randomCountClimate))
    print("\nrandomCountHillary: "+ str(randomCountHillary))
    print("\nrandomCountFeminism: "+ str(randomCountFeminism))

def makeallThreeBal(balforAgainst,noStanceAllthree,AllthreeBal):
    makeStanceNoStance(balforAgainst,noStanceAllthree,AllthreeBal)
    
    
def makeStanceNoStance(balforAgainst,noStance, StanceNoStanceFile):
    forAgainstList= file_utilities.readCsv(balforAgainst, "utf-8", ",")
    noStanceList=file_utilities.readCsv(noStance, "utf-8", ",")
    StanceNoStanceList=forAgainstList+noStanceList
    random.shuffle(StanceNoStanceList)
    cols=StanceNoStanceList[0].keys()
    file_utilities.writeCsv(StanceNoStanceFile, StanceNoStanceList, cols) 
    
def gettopiccountforRandom(filename,othertopiccount):
    if "climate" in filename:
            return  othercountclimate
    else:
            return othertopiccount                             

def makeNostanceTopic(noStanceFile,noStanceAllThreeFile,filelist,randomtweets,othertopiccount,randomcount):
    Nonelist=[]
    NonelistAllthree=[]
    randomlist=file_utilities.readCsv(randomtweets, "utf-8", ",")
    random.shuffle(randomlist) 
    for filename in filelist:
        forlist=[]
        againstlist=[]
        rowdicts=file_utilities.readCsv(filename, "utf-8", ",")
        for row in rowdicts:
                if row["Stance"].lower()=="favor":
                    forlist.append(row)
                if row["Stance"].lower()=="against":
                    againstlist.append(row)
        random.shuffle(forlist)  
        random.shuffle(againstlist) 
        countothers=gettopiccountforRandom(filename,othertopiccount)
        numbers=int(countothers/2.0)
        forlistNostance=forlist[0:numbers]
        againstlist=againstlist[0:numbers]
        Nostancelist=forlistNostance+ againstlist
        NostanceallthreeNum=int(numbers/2.0)
        Nostanceallthree=forlist[0:NostanceallthreeNum]+againstlist[0:NostanceallthreeNum] 
        for row in Nostancelist:
            row["Stance"]="NONE"
            
        for row in Nostanceallthree:
            row["Stance"]="NONE"    
        Nonelist= Nonelist + Nostancelist  
        NonelistAllthree= NonelistAllthree + Nostanceallthree
    Nonelist=Nonelist+randomlist[0:randomcount] 
    NonelistAllthree=NonelistAllthree+randomlist[0:int(randomcount/2.0)] 
    cols=Nonelist[0].keys()
    random.shuffle(Nonelist)
    file_utilities.writeCsv(noStanceFile, Nonelist, cols) 
    file_utilities.writeCsv(noStanceAllThreeFile, NonelistAllthree, cols)
    
def run(section):
    random.seed(10)
    config = configparser.ConfigParser()
    parentdir= os.path.dirname(os.getcwd())
    config.read(os.path.join(parentdir,'docs','configFiles','PreprocessStepsConfig.ini'))
    
    abortionbal=config.get(section, "abortionbal")
    atheismbal=config.get(section,  "atheismbal")
    climatebal=config.get(section, "climatebal")
    feminismbal=config.get(section, "feminismbal")
    hillarybal=config.get(section, "hillarybal")
    randomtweets=config.get(section, "randomtweets")
    
    noStanceAbortion=config.get(section,"NoStanceAbortion")
    noStanceAtheism=config.get(section,"NoStanceAtheism")
    noStanceClimate=config.get(section,"NoStanceClimate")
    noStanceHillary=config.get(section,"NoStanceHillary")
    noStanceFeminism=config.get(section,"NoStanceFeminism")
    
    randomcountAbortion=int(config.get(section,"randomcountabortion"))
    randomcountAtheism=int(config.get(section,"randomcountAtheism"))
    randomcountClimate=int(config.get(section,"randomcountClimate"))
    randomcountHillary=int(config.get(section,"randomcountHillary"))
    randomcountFeminism=int(config.get(section,"randomcountFeminism"))
    
    otherTopiccountAbortion=int(config.get(section,"OtherTopiccountAbortion"))
    otherTopiccountAtheism=int(config.get(section,"OtherTopiccountAtheism"))
    otherTopiccountClimate=int(config.get(section,"OtherTopiccountClimate"))
    otherTopiccountFeminism=int(config.get(section,"OtherTopiccountFeminism"))
    otherTopiccountHillary=int(config.get(section,"OtherTopiccountHillary"))
    
    
    stanceNoStanceAbortionFile=config.get(section,"StanceNoStanceAbortion")
    stanceNoStanceAtheismFile=config.get(section,"StanceNoStanceAtheism")
    stanceNoStanceClimateFile=config.get(section,"StanceNoStanceClimate")
    stanceNoStanceHillaryFile=config.get(section,"StanceNoStanceHillary")
    stanceNoStanceFeminismFile=config.get(section,"StanceNoStanceFeminism")
    
    allthreeAbortion= config.get(section,"AllthreeAbortion")
    allthreeAtheism=config.get(section,"AllthreeAtheism")
    allthreeClimate=config.get(section,"AlthreeClimate")
    allthreeHillary=config.get(section,"AllthreeHillary")
    allthreeFeminism=config.get(section,"AllthreeFeminism")
    
    filelistAbortion=[atheismbal, climatebal, feminismbal,hillarybal]
    noStanceAllThreeAbortion=noStanceAbortion[:-4] +"AllThree.csv"
    makeNostanceTopic(noStanceAbortion,noStanceAllThreeAbortion,filelistAbortion,randomtweets,otherTopiccountAbortion,randomcountAbortion)
    
    noStanceAllThreeAtheism=noStanceAtheism[:-4]+"AllThree.csv"
    filelistAtheism=[abortionbal, climatebal, feminismbal,hillarybal]
    makeNostanceTopic(noStanceAtheism,noStanceAllThreeAtheism,filelistAtheism,randomtweets,otherTopiccountAtheism,randomcountAtheism)
    
    
    noStanceAllThreeClimate=noStanceClimate[:-4] +"AllThree.csv"
    filelistClimate=[abortionbal, atheismbal, feminismbal,hillarybal]
    makeNostanceTopic(noStanceClimate,noStanceAllThreeClimate,filelistClimate,randomtweets,otherTopiccountClimate,randomcountClimate)
    
    
    noStanceAllThreeFeminism=noStanceFeminism[:-4] +"AllThree.csv"
    filelistFeminism=[abortionbal, climatebal, atheismbal,hillarybal]
    makeNostanceTopic(noStanceFeminism,noStanceAllThreeFeminism,filelistFeminism,randomtweets,otherTopiccountFeminism,randomcountFeminism)
    
    
    noStanceAllThreeHillary=noStanceHillary[:-4] +"AllThree.csv"
    filelistHillary=[abortionbal, climatebal, feminismbal,atheismbal]
    makeNostanceTopic(noStanceHillary,noStanceAllThreeHillary,filelistHillary,randomtweets,otherTopiccountHillary,randomcountHillary)
    
    
    makeStanceNoStance(abortionbal,noStanceAbortion,stanceNoStanceAbortionFile)
    makeStanceNoStance(atheismbal,noStanceAtheism,stanceNoStanceAtheismFile)
    makeStanceNoStance(climatebal,noStanceClimate,stanceNoStanceClimateFile)
    makeStanceNoStance(feminismbal,noStanceFeminism,stanceNoStanceFeminismFile)
    makeStanceNoStance(hillarybal,noStanceHillary,stanceNoStanceHillaryFile)
    
    makeallThreeBal(abortionbal,noStanceAllThreeAbortion,allthreeAbortion)
    makeallThreeBal(atheismbal,noStanceAllThreeAtheism,allthreeAtheism)
    makeallThreeBal(climatebal,noStanceAllThreeClimate,allthreeClimate)
    makeallThreeBal(hillarybal,noStanceAllThreeHillary,allthreeHillary)
    makeallThreeBal(feminismbal,noStanceAllThreeFeminism,allthreeFeminism)
    
    



if __name__ == '__main__':
    section="stanceNoStance"
    calculaterandomtopic(section)