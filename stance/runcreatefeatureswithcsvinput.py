'''
Created on Feb 25, 2016
Reading parameters from configFile is time cconsuming.
One can do simultaneous runs by giving parameters in a csv instead
@author: amita
'''
import createFeatures ,pandas as pd
import configparser
from stance.file_utilities import get_config_file

def readCongigFile(section):
    config = configparser.ConfigParser()
    config.read(get_config_file('runcreatefeaturescsvinput_Config.ini'))
    inputdata=config.get(section,"csv_with_parameters")
    return inputdata

def run(section):
    args=readCongigFile(section)
    executewithcsv(args)

def executewithcsv(inputdata):
    # Make sure topic is valid
    inputdata_df=pd.read_csv(inputdata,na_filter=False)
    for index, row in inputdata_df.iterrows():
        done=int(row["done"])
        if done==1:
            continue
        inputFile=row["inputFile"]
        outputFile=row["outputFile"]
        textCol=row["textCol"]
        stanceCol=row["stanceCol"]
        mode=row["mode"]
        train_file=row["train_file"]
        stem=str(row["stem"]).title()
        stop=str(row["stop"]).title()
        pmiPoolPercent=row["pmiPoolPercent"]
        predictedVar=row["predictedVar"]
        featureList=row["featureList"].split(",")
        split=row["split"]
        target=row["target"] 
        ngram_order=row["ngram_order"] 
        dependencyFile=row["dependencyFile"]
        dependency=str(row["dependency"]).split(",")
        topic=row["topic"]
        assert(topic == "legalization of abortion" or topic == "climate change is a real concern" or topic == "atheism" or topic == "hillary clinton" or topic == "feminist movement")
        createFeatures.execute(inputFile, outputFile, textCol, stanceCol, mode, train_file, stem,stop, pmiPoolPercent, predictedVar,featureList, split, target, ngram_order, dependencyFile, dependency, topic)

    
    
if __name__ == '__main__':
    section="alltopicsNohashtags"
    run (section)
    section="alltopicsV2"
    run (section)