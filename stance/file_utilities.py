#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created on Oct 15, 2015
write some common file handling routines
@author: amita
'''


import csv, codecs, os, random
import json
from copy import deepcopy

PROJECT_DIR = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
RESOURCE_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "resources"))
CONFIG_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "docs", "configFiles"))
PARSE_DIR = os.path.abspath(os.path.join(PROJECT_DIR, "parses"))


def load_resource(path_to_file):
    """
    Get the full path to some file in the resources directory.
    :param path_to_file: Path to the file from within the resources directory. If your file is resources/test.txt, then
    pass test.txt, not resources/test.txt.
    :raise: FileNotFoundError
    :return: The full path to your requested file
    """
    path = os.path.join(RESOURCE_DIR, path_to_file)
    if os.path.exists(path):
        # return file path
        return path
    else:
        # raise error
        raise FileNotFoundError("File: {0} does not exists".format(path))


def get_config_file(config_file_name):
    """
    Gets the absolute path to a config file by name
    :param config_file_name: The name of a config file: e.g. createFeaturesConfig.ini,
    NOT docs/configFiles/createFeaturesConfig.ini
    :return:
    """
    path = os.path.join(CONFIG_DIR, config_file_name)
    if os.path.exists(path):
        return path
    else:
        # raise error
        raise FileNotFoundError("File: {0} does not exists".format(path))


#rows: list of dictionaries
def removenulldata(rows):
    allRows=[]
    for row in rows:
        text=row["Tweet"]
        if "\0" in text:
            text=text.replace("\0","")
        row["Tweet"]= text
        allRows.append(row)
    return allRows


#input: name of file along with extension
def tablimitedtoCSV(txt_file,csv_file):

    # use 'with' if the program isn't going to immediately terminate
    # so you don't leave files open
    # the 'b' is necessary on Windows
    # it prevents \x1a, Ctrl-z, from ending the stream prematurely
    # and also stops Python converting to / from different line terminators
    # On other platforms, it has no effect
    in_txt = csv.reader(open(txt_file, "rb"), delimiter = '\t')
    out_csv = csv.writer(open(csv_file, 'wb'))
    out_csv.writerows(in_txt)


# allRows:list of dictionaries
def addUniqueRowCol(allRows):
    count=1
    allNewRows=[]
    for row in allRows:
        Newrow=deepcopy(row)
        Newrow["uniqueRowNo"]=count # gives each row a unique number for Mechanical turk
        count=count+1
        allNewRows.append(Newrow)
    return allNewRows



#takes an input json, gives the rowdicts
def jsonToRowDicts(listofdictfile):
    with open(listofdictfile, 'r',encoding='utf-8') as fp:
        rowdicts = json.load(fp)
    return  rowdicts

# write a list of dictionaries to a json file
def  writeJson(list_dict,filename):
    with open(filename, 'w') as f:
        json.dump(list_dict, f)


def readCsvdelimiterTab(inputFile):
    inputfile = codecs.open(inputFile,'r',encoding='latin-1')
    result = list(csv.DictReader(inputfile,delimiter='\t'))
    return result

def readCsv(inputcsv,encod,delimit):
    inputfile = codecs.open(inputcsv,'r',encoding=encod)
    result = list(csv.DictReader(inputfile, delimiter =delimit ))
    return result


def read_csv_header(inputcsv, encod, delimit):
    with codecs.open(inputcsv, 'r', encoding=encod) as fh:
        return csv.DictReader(fh, delimiter=delimit).fieldnames


# write a csv, rowdicts:list of dictionaries,
#Assumption:  header is present in colnames
def writeCsv(outputcsv, rowdicts,colnames):
    restval=""
    extrasaction="ignore"
    dialect="excel"
    outputfile = codecs.open(outputcsv ,'w',encoding='utf-8' )
    csv_writer = csv.DictWriter(outputfile, colnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC)
    csv_writer.writeheader()
    csv_writer.writerows(rowdicts)
    outputfile.close()


def readTextFile(Filename):
    f = codecs.open(Filename, "r", encoding='utf-8')
    TextLines=f.readlines()
    f.close()
    return TextLines

def writeTextFile(Filename,Lines):
    f = codecs.open(Filename, "w",encoding='utf-8')
    f.writelines(Lines)
    f.close()

def write_csv_header_unkown(filename, rowdicts, fieldnames=None, include_sorted_remaining_fields=True, get_keys_from_first_row=False, include_header=True, restval="", extrasaction="ignore", dialect="excel", *args, **kwds):
    """
    @note: Differs in the default extrasaction value
    @note: include_sorted_remaining_fields=True is expensive, so use get_keys_from_first_row=True when possible
    #TODO: Allow wildcards "*"
    """

    if fieldnames == None:
        fieldnames = []
    if include_sorted_remaining_fields:
        original_fields = set(fieldnames)
        additional_fields = set()
        for row in rowdicts:
            for key in row.keys():
                if key not in original_fields:
                    additional_fields.add(key)
            if get_keys_from_first_row:
                break
        fieldnames.extend(sorted(additional_fields))

    if "target" not in fieldnames:
        fieldnames.append("target")
    else:
        # move target to last entry for weka convenience
        fieldnames.remove("target")
        fieldnames.append("target")

    directory = os.path.dirname(filename)
    if not os.path.exists(directory):
        os.makedirs(directory)

    outputfile = codecs.open(filename,'w','utf-8')
    csv_writer = csv.DictWriter(outputfile, fieldnames, restval, extrasaction, dialect, quoting=csv.QUOTE_NONNUMERIC, *args, **kwds)

    if include_header:
        #python 2.7 has this, 2.6 does not, this provides backwards compatibility
        if hasattr(csv_writer, 'writeheader'):
            csv_writer.writeheader()
        else:
            header = dict(zip(fieldnames, fieldnames))
            rowdicts.insert(0, header)

    return csv_writer.writerows(rowdicts)

#randomly samples from the larger tweetset
def balance(positive_tweets, negative_tweets):
    if len(positive_tweets) > len(negative_tweets):
        balanced_positive = random.sample(positive_tweets, len(negative_tweets))
        return balanced_positive + negative_tweets
    else:
        balanced_negative = random.sample(negative_tweets, len(positive_tweets))
        return positive_tweets + balanced_negative


def convertJsonToCsv( InputFileJson,OutputCSV):
    rowdicts=jsonToRowDicts(InputFileJson)
    fieldnames= sorted(rowdicts[0].keys())
    writeCsv(OutputCSV, rowdicts, fieldnames)


def convertCsvToJson( InputFileCsv,OutputJson):
    rowdicts=readCsv(InputFileCsv)
    writeJson(rowdicts,OutputJson)

if __name__ == '__main__':
    text="/Users/amita/git/stance/resources/AFINN-111.txt"
    readTextFile(text)


