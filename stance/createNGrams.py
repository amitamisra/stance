#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''
Created Oct 16 2015
Creates feature files of unigrams or bigrams
Input: CSV file with tweet column
Output: ARFF and CSV files
@author: Theodore Handleman
'''

import csv, codecs, os, random, sys, nltk, re
import arff_writer
from nltk.tokenize import RegexpTokenizer
import nltk.stem
from collections import Counter

# reads the csv using file_utilities and seperates them into
# categorie"s specified for the config files
# options: split - "stance" or "polar"


def processDict(rowdict, featureVector, split, stanceCol):

            featureVector[stanceCol] = rowdict[stanceCol]

def feature_score(token_dict, count, stem, feature):
    repeat, upper = False, False
    if token_dict["repeatedOccurence"]:
        repeat = True
    elif token_dict["allCaps"]:
        upper = True

    if stem == 'True':
        if repeat or upper:
            count[token_dict["corrected_token_stem"].lower()] += 1.5
        else:
            count[token_dict["corrected_token_stem"].lower()] += 1
    else:
        if repeat or upper:
            count[token_dict["corrected_token"].lower()] += 1.5
        else:
            count[token_dict["corrected_token"].lower()] += 1

def create_ngram_dicts(tokens, n, stem):
    n_grams = []

    # print("n: " + str(n))
    # prefix = ''
    # if n == 1:
    #     prefix = 'Unigram:'
    # elif n == 2:
    #     prefix = 'Bigram:'
    # elif n == 3:
    #     prefix = 'Trigram:'

    n_dicts = [tuple(tokens[i:i+n]) for i in range(0 , len(tokens) - n + 1)]
    for n_tuple in n_dicts:
        n_gram = {}

        if stem == "True":
            n_tokens = [n_tuple[i]["corrected_token_stem"] for i in range(0, n)]
            n_gram["corrected_token_stem"] = '___'.join(n_tokens)
        else:
            n_tokens = [n_tuple[i]["corrected_token"] for i in range(0, n)]
            n_gram["corrected_token"] = '___'.join(n_tokens)

        n_gram["allCaps"] = all(n_tuple[i]["allCaps"] == True for i in range(0, n))
        n_gram["repeatedOccurence"] = any(n_tuple[i]["repeatedOccurence"] == True for i in range(0, n))
        n_grams.append(n_gram)

        #print(n_gram)
    return n_grams


def add_text_feature(rowdict, featureVector, features, stem, stop, stopset):
    for feature in features:
        count = Counter()
        for token_dict in rowdict["tokens"]:
            stemmed = stem_token(token_dict["corrected_token"])
            token_dict["corrected_token_stem"] = stemmed

        if feature == 1:
            stop_filter = [token_dict for token_dict in rowdict["tokens"] if token_dict["corrected_token"].lower() not in stopset]
            if stop == "True":
                for token_dict in stop_filter:
                    feature_score(token_dict, count, stem, feature)
            else:
                for token_dict in rowdict["tokens"]:
                    feature_score(token_dict, count, stem, feature)
        if feature == 2 or feature == 3:
            ngram_dicts = create_ngram_dicts(rowdict["tokens"], feature, stem)
            for token_dict in ngram_dicts:
                feature_score(token_dict, count, stem, feature)

        for token in count:
            feature_tok = ''
            if feature == 1:
                feature_tok =  "Unigram:" + token
            elif feature == 2:
                feature_tok = "Bigram:" + token
            else:
                feature_tok = "Trigram:" + token

            feature_tok=replaceForweka(feature_tok)
            featureVector[feature_tok] = count[token]

def add_dependency_feature(rowdict, featureVector, generalization):
    count = Counter()

    for dependency in rowdict["tweebo_parse"]:
        head = rowdict["tweebo_parse"][dependency]["head"]
        if head == 0 or head == -1:
            continue
        else:
            validateDependency(rowdict, head, dependency)
            if 'general' in generalization:
                dep_feature = 'dep_' + rowdict["tweebo_parse"][head]["form"].lower() + '_' + rowdict["tweebo_parse"][dependency]["form"].lower()
                count[dep_feature] += 1
            if 'liwc' in generalization:
                for cat in rowdict["tokens"][head - 1]["category_list"]:
                    dep_feature_gov_liwc = 'LIWC:dep_' + cat.lower() + '_' + rowdict["tweebo_parse"][dependency]["form"].lower()
                    count[dep_feature_gov_liwc] += 1
                for cat in rowdict["tokens"][dependency - 1]["category_list"]:
                    dep_feature_dep_liwc = 'LIWC:dep_' + rowdict["tweebo_parse"][head]["form"].lower() + '_' + cat.lower()
                    count[dep_feature_dep_liwc] += 1
            if 'sentiment_afinn' in generalization:
                category = 'afinnscore'
                sentimentDependency(rowdict, head, dependency, category, count)
            if 'sentiment_binglui' in generalization:
                category = 'bingluiscore'
                sentimentDependency(rowdict, head, dependency, category, count)
            if 'sentiment_combined' in generalization:
                category = 'combinedscore'
                sentimentDependency(rowdict, head, dependency, category, count)

    for token in count:
        featureVector[token] = count[token]

def sentimentDependency(rowdict, head, dependency, category, count):
    if rowdict["tokens"][head - 1][category] != 0:

        score=rowdict["tokens"][head - 1][category]
        dep_feature_gov_sentiment = category + ':dep_' + scoreToSentiment(score) + '_' + rowdict["tweebo_parse"][dependency]["form"].lower()

        count[dep_feature_gov_sentiment] += 1
    if rowdict["tokens"][dependency - 1][category] != 0:
        score=rowdict["tokens"][dependency - 1][category]
        dep_feature_dep_sentiment = category + ':dep_' + rowdict["tweebo_parse"][head]["form"].lower() + '_' + scoreToSentiment(score)
        count[dep_feature_dep_sentiment] += 1

def scoreToSentiment(sentiment_val):
    if sentiment_val < 0:
        return 'negative'
    elif sentiment_val > 0:
        return 'positive'

def validateDependency(rowdict, head, dependency):
    head_token = rowdict["tweebo_parse"][head]["form"]
    dependency_token = rowdict["tweebo_parse"][dependency]["form"]

    if head_token != rowdict["tokens"][head - 1]["original_token"]:
        print("head_token: {0}, corresponding index token: {1}".format(head_token, rowdict["tokens"][head - 1]["original_token"]))
        sys.exit("Head token does not match token_dict token at corresponding index")
    if dependency_token != rowdict["tokens"][dependency - 1]["original_token"]:
        print("dep_token: '{0}', corresponding index token: '{1}'".format(dependency_token, rowdict["tokens"][dependency - 1]["original_token"]))
        sys.exit("Dependency token does not match token_dict token at corresponding index")

def replaceForweka(feature_tok):
    feature_tok=feature_tok.replace("'",'Squote')
    feature_tok=feature_tok.replace('"','Dquote')
    return feature_tok

def add_pos_feature(rowdict, min_n, max_n, featureVector):
    for n in range(min_n,max_n+1):
        count = Counter()
        tokens = [token["pos"] for token in rowdict["tokens"]]
        n_grams = [tuple(tokens[i:i+n]) for i in range(0 , len(tokens) - n + 1)]

        pos_tokens = []
        for n_gram in n_grams:
            tuple_tokens = [n_gram[i] for i in range(0, n)]
            token = '___'.join(tuple_tokens)
            count[token] += 1

        # bigrams = nltk.bigrams(tokens)
        # tokenized_bigrams = [token[0] + '___' + token[1] for token in bigrams]

        # for token in tokenized_bigrams:
        #     count[token] += 1

        if n==2:
            for token in count:
                featureVector["POSBigram:" + token] = count[token]
        elif n==3:
            for token in count:
                featureVector["POSTrigram:" + token] = count[token]

def stem_token (token):
    stemmer = nltk.stem.PorterStemmer()
    return stemmer.stem(token)

#writes ARFF files for weka to use and csv files
#csv files are oddly formatted at the moment
def writefiles(filename, attributes, vectors,predictedVar):
    arff_writer.write(filename, vectors,
            classification_feature=predictedVar, features=attributes)

