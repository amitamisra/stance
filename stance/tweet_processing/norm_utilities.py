import nltk
import re
from itertools import zip_longest

import enchant

from stance import file_utilities


def check_repeat(token, pos):
    """
    Checks for repeated characters in a token that might indicate stressing the token
    @param token:
    @param pos:
    @return:
    """
    prev_char = ''
    char_count, curr_index, start_index = 0, 0, 0
    elong_list = []

    ignore = ['@', '~', ',', 'U', 'E']

    if pos in ignore or re.match(r'[0-9,]+', token) or token[0] == '@':
        return []

    for char in token:
        if char == prev_char:
            if char_count < 3:
                if char_count == 0:
                    start_index = curr_index - 1
                    char_count += 2
                else:
                    char_count += 1
        else:
            if char_count > 2:
                elong_list.append((start_index + 2, curr_index))

            char_count = 0
            prev_char = char

        curr_index += 1

    if char_count > 2:
        elong_list.append((start_index + 2, -1))

    return elong_list


def reduce(token, elong_list):
    end_point = 0
    partition = False

    new_token = ''
    if elong_list is not None:
        for start, stop in elong_list:
            if not partition:
                new_token += token[0:start]
                partition = True
            else:
                new_token += token[end_point:start]

            end_point = stop

        if end_point <= len(token) - 1 and end_point != -1:
            new_token += token[end_point:]

    return new_token


def recurr_elim(token):
    """
    Remove recurring characters in a token
    @param token:
    @param pos:
    @return:
    """
    mod_list = check_repeat(token["corrected_token"], token["pos"])
    if len(mod_list) > 0:
        token["corrected_token"] = reduce(token["corrected_token"], mod_list)
        token["emphasized"] = True
    else:
        token["emphasized"] = False


stemmer = nltk.stem.PorterStemmer()


def stem_token (token):
    return stemmer.stem(token)


def normalize_token(token, remove_hashtags=True):
    """
    Normalizes a token by removing
    @param token: A token as a dictionary
    @param remove_hashtags: True to generalize all hashtags to "#HASHTAG"
    @return:
    """
    ignore = ['#', '~', ',', 'E', '^']
    token["corrected_token"] = token["original_token"].replace("\\\'", "\'")
    recurr_elim(token)
    str_token = token["corrected_token"]
    if str_token[:2] == 'b\'' or str_token[:2] == 'b\"':
        str_token = replace_token(str_token[2:])
    elif str_token == 'RT':
        str_token = ' '
    elif token["pos"] == '@' or str_token[0] == '@':
        str_token = 'user_ref'
    elif token["pos"] == 'U':
        str_token = "url"
    else:
        if token["pos"] not in ignore:
            str_token = replace_token(str_token)
    token["corrected_token"] = str_token
    token["stemmed"] = stem_token(str_token)
    if len(token["original_token"]) > 0 and token["original_token"][0] == "#":
        token["corrected_token"] = "#HASHTAG"
        token["stemmed"] = "#HASHTAG"
        return token
    return token


def default_normalization_dict():
    return file_utilities.load_resource("emnlp2012-lexnorm/emnlp_dict.txt")


def build_slang_dict():
    filename = default_normalization_dict()
    slang_dict = {}
    slang_fh = open(filename, 'r')
    for line in slang_fh:
        slang_def = line.rstrip().split('\t')
        slang_dict[slang_def[0]] = slang_def[1]

    return slang_dict


SPELLER = enchant.Dict('en_us')
SLANG_DICT = build_slang_dict()


def replace_token(token, slang_dict=SLANG_DICT):
    uppers = [True if c.isupper() else False for c in token]
    lower_token = token.lower()
    if len(token) > 0 and token[0] != "#" and not SPELLER.check(lower_token):
        if lower_token in slang_dict:
            if len(slang_dict[lower_token].split(' ')) == 1:
                replacement = slang_dict[lower_token]
                result = ""
                for upper, c in zip_longest(uppers, replacement):
                    if c is not None:
                        if upper:
                            c = c.upper()
                        result += c

                return result

    return token
