#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-
""" Usage: Because we can't run this from a subprocess, there are a few steps:
    Requirements: Download and install the TweeboParser to resources folder. Python 2
    future package must be installed
    1. Make sure your virtual environment is not activated.
    2. Run the script
    3. Now add the dependency file to the respective option in createFeaturesConfig.ini
       The name of the dependency file is the same as the generated txt file except is has '.predict' appended to it.
    5. Activate your virtual environment and run createFeatures.py
"""
import subprocess

import sys

from stance import file_utilities
from stance.file_utilities import PARSE_DIR

import hashlib
import os
import os.path
import shutil
import tempfile


def get_parses(tweets):
    """
    Parses the tweets. This requires creating a temporary file and then creating a parse file from it.
    After file creation we can read the parses. This will automatically store parses in the PROJECT_DIR/parses
    directory, and it will load them instead of reparsing if the same exist list of tweets is passed in.
    @param tweets: A list of tweets as strings
    @return: A list of tweet parses
    """
    # Write tweets to text file, line-by-line
    parse_filename = hashlib.md5("".join(tweets).encode("utf-8")).hexdigest() + ".parses"
    output_file = os.path.join(PARSE_DIR, parse_filename)
    if not os.path.exists(output_file):
        print("Parses do not exist... ")
        tmp_txt_file = tempfile.NamedTemporaryFile("w")
        tweets_by_line = "\n".join(tweets) + "\n"
        tmp_txt_file.writelines(tweets_by_line)
        tmp_txt_file.flush()

        # Do parsing
        current_dir = os.getcwd()
        try:
            parser_path = file_utilities.load_resource("TweeboParser")
        except FileNotFoundError:
            print("TweeboParser not found in the resources directory. You need to install it there. Check the readme.")
            print("do `git clone https://github.com/ikekonglp/TweeboParser` inside resources and then run install.sh")
            sys.exit(1)

        os.chdir(parser_path)
        subprocess.call("./run.sh " + tmp_txt_file.name, shell=True)
        os.chdir(current_dir)
        tmp_txt_file.close()
        prediction_file = tmp_txt_file.name + ".predict"
        shutil.move(prediction_file, output_file)
    else:
        print("Parses already exist. Loading from file... ")

    # Read the parses from a file
    raw_tweet_parses = split_file(output_file)
    return [create_parse_dict(raw_parse) for raw_parse in raw_tweet_parses if raw_parse != [""]]


def split_file(filename):
    total_lines = ''
    with open(filename, "r", encoding="utf-8") as fh:
        for line in fh.readlines():
            total_lines += line
    tweets = total_lines.split("\n\n")

    tweet_parses = []
    for tweet in tweets:
        tweet_parses.append(tweet.split("\n"))
    return tweet_parses


def create_parse_dict(tweet_parse):
    parse = {}
    for token_parse in tweet_parse:
        values = token_parse.split('\t')
        parse[int(values[0])] = {"form": values[1], "pos": values[4],
                                 "head": int(values[6]), "relation": values[7]}
    return parse
