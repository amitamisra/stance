import os
from file_utilities import PARSE_DIR

if not os.path.exists(PARSE_DIR):
    os.makedirs(PARSE_DIR)


def process_tweets(tweets, parse_file=None, normalize_tokens=True, remove_hashtags=True):
    """
    Tokenize, pos tag, normalize, and parse tweets. Can specify a parse file to load from if the parses have
    already been creating using the CMU TweeboParser. In the case that a parse file is absent, the parses will
    be created, and then they will be saved in the {PROJECT}/parses directory, so that subsequent runs will
    not recreate them.
    @param tweets: A list of tweets as strings
    @param parse_file: A file containing preparsed tweets as produced by tweet_parsing.
    @param normalize_tokens: True to normalize the tokens by attempting to correct spelling, False to leave them alone
    @return: a list of dictionaries such as [{tweet = "the tweet", tokens=[...] parses=[...] ]
    """
    from stance.tweet_processing import norm_utilities
    from stance.tweet_processing import tweet_parsing
    from stance.tweet_processing import tweet_tagging

    token_lists = list()
    for token_list in tweet_tagging.runtagger_parse(tweets):
        new_token_list = list()
        for index, token_tup in zip(range(0, len(token_list)), token_list):
            token_dict = {"token_index": index,
                          "original_token": token_tup[0],
                          "pos": token_tup[1],
                          }
            if normalize_tokens:
                norm_utilities.normalize_token(token_dict, remove_hashtags)
            new_token_list.append(token_dict)

        token_lists.append(new_token_list)

    if parse_file is not None:
        tweet_parses = [tweet_parsing.create_parse_dict(tweet_parse)
                        for tweet_parse in tweet_parsing.split_file(parse_file)]
    else:
        tweet_parses = tweet_parsing.get_parses(tweets)

    return [{"tweet": tweet, "tokens": tokens, "parse": parse}
            for tweet, tokens, parse in zip(tweets, token_lists, tweet_parses)]

if __name__ == "__main__":
    import pprint

    pp = pprint.PrettyPrinter()
    pp.pprint(process_tweets(["This is a test tweet", "Down with @Hillary!"]))
