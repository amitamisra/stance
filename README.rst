===============================
stance16
===============================

.. image:: https://img.shields.io/travis/amitamisra/stance.svg
        :target: https://travis-ci.org/amitamisra/stance

.. image:: https://img.shields.io/pypi/v/stance.svg
        :target: https://pypi.python.org/pypi/stance


stance classification of tweets

* Free software: ISC license
* Documentation: https://stance.readthedocs.org.

Features
--------

* TODO
